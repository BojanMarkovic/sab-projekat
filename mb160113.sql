CREATE DATABASE [kurirskaSluzba]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'kurirskaSluzba', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\kurirskaSluzba.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'kurirskaSluzba_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\kurirskaSluzba_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [kurirskaSluzba] SET COMPATIBILITY_LEVEL = 150
GO
ALTER DATABASE [kurirskaSluzba] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET ARITHABORT OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET AUTO_CREATE_STATISTICS ON(INCREMENTAL = OFF)
GO
ALTER DATABASE [kurirskaSluzba] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [kurirskaSluzba] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [kurirskaSluzba] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET  DISABLE_BROKER 
GO
ALTER DATABASE [kurirskaSluzba] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [kurirskaSluzba] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [kurirskaSluzba] SET  READ_WRITE 
GO
ALTER DATABASE [kurirskaSluzba] SET RECOVERY FULL 
GO
ALTER DATABASE [kurirskaSluzba] SET  MULTI_USER 
GO
ALTER DATABASE [kurirskaSluzba] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [kurirskaSluzba] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [kurirskaSluzba] SET DELAYED_DURABILITY = DISABLED 
GO
USE [kurirskaSluzba]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = Off;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = Primary;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = On;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = Primary;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = Off;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = Primary;
GO
USE [kurirskaSluzba]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [kurirskaSluzba] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO


CREATE TABLE [administrator]
( 
	[IdK]                integer  NOT NULL 
)
go

ALTER TABLE [administrator]
	ADD CONSTRAINT [XPKadministrator] PRIMARY KEY  CLUSTERED ([IdK] ASC)
go

CREATE TABLE [Adresa]
( 
	[IdA]                integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Ulica]              varchar(100)  NOT NULL ,
	[Broj]               integer  NOT NULL 
	CONSTRAINT [Default_Value_294_1788714846]
		 DEFAULT  0,
	[Xkoordinata]        integer  NOT NULL ,
	[Ykoordinata]        integer  NOT NULL ,
	[IdG]                integer  NOT NULL 
)
go

ALTER TABLE [Adresa]
	ADD CONSTRAINT [XPKAdresa] PRIMARY KEY  CLUSTERED ([IdA] ASC)
go

CREATE TABLE [Grad]
( 
	[IdG]                integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Naziv]              varchar(100)  NOT NULL ,
	[Postanski_broj]     varchar(100)  NOT NULL 
)
go

ALTER TABLE [Grad]
	ADD CONSTRAINT [XPKGrad] PRIMARY KEY  CLUSTERED ([IdG] ASC)
go

ALTER TABLE [Grad]
	ADD CONSTRAINT [XAK2Grad] UNIQUE ([Postanski_broj]  ASC)
go

CREATE TABLE [Korisnik]
( 
	[IdK]                integer  IDENTITY ( 1,1 )  NOT NULL ,
	[ime]                varchar(100)  NOT NULL ,
	[prezime]            varchar(100)  NOT NULL ,
	[korisnicko_ime]     varchar(100)  NOT NULL ,
	[sifra]              varchar(100)  NOT NULL ,
	[adresa]             integer  NULL 
)
go

ALTER TABLE [Korisnik]
	ADD CONSTRAINT [XPKKorisnik] PRIMARY KEY  CLUSTERED ([IdK] ASC)
go

ALTER TABLE [Korisnik]
	ADD CONSTRAINT [XAK1Korisnik] UNIQUE ([korisnicko_ime]  ASC)
go

CREATE TABLE [kurir]
( 
	[IdK]                integer  NOT NULL ,
	[broj_isporucenih_paketa] integer  NULL 
	CONSTRAINT [Default_Value_294_1670208921]
		 DEFAULT  0,
	[profit]             decimal(10,3)  NULL 
	CONSTRAINT [Default_Value_309_691421305]
		 DEFAULT  0.0,
	[status]             integer  NULL 
	CONSTRAINT [Default_Value_294_455620236]
		 DEFAULT  0
	CONSTRAINT [Validation_Rule_428_559996357]
		CHECK  ( [status]=0 OR [status]=1 ),
	[IdV]                integer  NULL ,
	[broj_vozacke_dozvole] varchar(100)  NOT NULL ,
	[tempProfit]         decimal(10,3)  NULL 
	CONSTRAINT [Default_Value_309_1193157877]
		 DEFAULT  0.0
)
go

ALTER TABLE [kurir]
	ADD CONSTRAINT [XPKkurir] PRIMARY KEY  CLUSTERED ([IdK] ASC)
go

ALTER TABLE [kurir]
	ADD CONSTRAINT [XAK1kurir] UNIQUE ([broj_vozacke_dozvole]  ASC)
go

CREATE TABLE [LokacijaMagacina]
( 
	[IdA]                integer  NOT NULL ,
	[IdM]                integer  IDENTITY ( 1,1 )  NOT NULL 
)
go

ALTER TABLE [LokacijaMagacina]
	ADD CONSTRAINT [XPKLokacijaMagacina] PRIMARY KEY  CLUSTERED ([IdM] ASC)
go

ALTER TABLE [LokacijaMagacina]
	ADD CONSTRAINT [XAK1LokacijaMagacina] UNIQUE ([IdA]  ASC)
go

CREATE TABLE [Paket]
( 
	[IdP]                integer  IDENTITY ( 1,1 )  NOT NULL ,
	[tip_paketa]         integer  NOT NULL 
	CONSTRAINT [Default_Value_294_473171691]
		 DEFAULT  0
	CONSTRAINT [Validation_Rule_433_471161491]
		CHECK  ( [tip_paketa]=0 OR [tip_paketa]=1 OR [tip_paketa]=2 OR [tip_paketa]=3 ),
	[tezina]             decimal(10,3)  NOT NULL 
	CONSTRAINT [Default_Value_309_1061250930]
		 DEFAULT  0.0
	CONSTRAINT [Validation_Rule_325_1229783522]
		CHECK  ( tezina >= 0.0 ),
	[cena]               decimal(10,3)  NULL 
	CONSTRAINT [Default_Value_309_1666092147]
		 DEFAULT  0.0
	CONSTRAINT [Validation_Rule_325_1432903506]
		CHECK  ( cena >= 0.0 ),
	[vreme_kreiranja]    datetime  NOT NULL ,
	[vreme_prihvatanja]  datetime  NULL ,
	[adresa_polaska]     integer  NOT NULL ,
	[adresa_dolaska]     integer  NOT NULL ,
	[trenutna_lokacija]  integer  NULL ,
	[status]             integer  NULL 
	CONSTRAINT [Default_Value_294_876363152]
		 DEFAULT  0
	CONSTRAINT [Validation_Rule_439_677705176]
		CHECK  ( [status]=0 OR [status]=1 OR [status]=2 OR [status]=3 OR [status]=4 ),
	[IdV]                integer  NULL ,
	[posiljalac]         integer  NOT NULL ,
	[potreba_za_dostavom] integer  NULL 
	CONSTRAINT [Default_Value_294_704310278]
		 DEFAULT  0
)
go

ALTER TABLE [Paket]
	ADD CONSTRAINT [XPKPaket] PRIMARY KEY  CLUSTERED ([IdP] ASC)
go

CREATE TABLE [ruta]
( 
	[IdA]                integer  NOT NULL ,
	[IdK]                integer  NOT NULL ,
	[IdR]                integer  IDENTITY ( 1,1 )  NOT NULL 
)
go

ALTER TABLE [ruta]
	ADD CONSTRAINT [XPKruta] PRIMARY KEY  CLUSTERED ([IdR] ASC)
go

CREATE TABLE [Vozilo]
( 
	[registracioni_broj] varchar(100)  NOT NULL ,
	[tip_goriva]         integer  NOT NULL 
	CONSTRAINT [Default_Value_294_977670991]
		 DEFAULT  0
	CONSTRAINT [Validation_Rule_446_679049640]
		CHECK  ( [tip_goriva]=0 OR [tip_goriva]=1 OR [tip_goriva]=2 ),
	[potrosnja]          decimal(10,3)  NOT NULL 
	CONSTRAINT [Default_Value_309_943071739]
		 DEFAULT  0.0
	CONSTRAINT [Validation_Rule_325_1075803542]
		CHECK  ( potrosnja >= 0.0 ),
	[nosivost]           decimal(10,3)  NOT NULL 
	CONSTRAINT [Default_Value_309_618388723]
		 DEFAULT  0.0
	CONSTRAINT [Validation_Rule_325_1093802116]
		CHECK  ( nosivost >= 0.0 ),
	[IdV]                integer  IDENTITY ( 1,1 )  NOT NULL ,
	[IdM]                integer  NULL ,
	[zauzet]             decimal(10,3)  NULL 
	CONSTRAINT [Default_Value_309_338385434]
		 DEFAULT  0.0
)
go

ALTER TABLE [Vozilo]
	ADD CONSTRAINT [XPKVozilo] PRIMARY KEY  CLUSTERED ([IdV] ASC)
go

ALTER TABLE [Vozilo]
	ADD CONSTRAINT [MyUniqueIndex2] UNIQUE ([registracioni_broj]  ASC)
go

CREATE TABLE [vozio]
( 
	[IdK]                integer  NOT NULL ,
	[IdV]                integer  NOT NULL 
)
go

ALTER TABLE [vozio]
	ADD CONSTRAINT [XPKvozio] PRIMARY KEY  CLUSTERED ([IdK] ASC,[IdV] ASC)
go

CREATE TABLE [zahtevi]
( 
	[IdK]                integer  NOT NULL ,
	[broj_vozacke_dozvole] varchar(100)  NOT NULL 
)
go

ALTER TABLE [zahtevi]
	ADD CONSTRAINT [XPKzahtevi] PRIMARY KEY  CLUSTERED ([IdK] ASC)
go

ALTER TABLE [zahtevi]
	ADD CONSTRAINT [XAK1zahtevi] UNIQUE ([broj_vozacke_dozvole]  ASC)
go


ALTER TABLE [administrator]
	ADD CONSTRAINT [R_8] FOREIGN KEY ([IdK]) REFERENCES [Korisnik]([IdK])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go


ALTER TABLE [Adresa]
	ADD CONSTRAINT [R_5] FOREIGN KEY ([IdG]) REFERENCES [Grad]([IdG])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [Korisnik]
	ADD CONSTRAINT [R_12] FOREIGN KEY ([adresa]) REFERENCES [Adresa]([IdA])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [kurir]
	ADD CONSTRAINT [R_7] FOREIGN KEY ([IdK]) REFERENCES [Korisnik]([IdK])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

ALTER TABLE [kurir]
	ADD CONSTRAINT [R_11] FOREIGN KEY ([IdV]) REFERENCES [Vozilo]([IdV])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [LokacijaMagacina]
	ADD CONSTRAINT [R_4] FOREIGN KEY ([IdA]) REFERENCES [Adresa]([IdA])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [Paket]
	ADD CONSTRAINT [R_13] FOREIGN KEY ([adresa_polaska]) REFERENCES [Adresa]([IdA])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

ALTER TABLE [Paket]
	ADD CONSTRAINT [R_14] FOREIGN KEY ([adresa_dolaska]) REFERENCES [Adresa]([IdA])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [Paket]
	ADD CONSTRAINT [R_15] FOREIGN KEY ([trenutna_lokacija]) REFERENCES [Adresa]([IdA])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [Paket]
	ADD CONSTRAINT [R_16] FOREIGN KEY ([IdV]) REFERENCES [Vozilo]([IdV])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

ALTER TABLE [Paket]
	ADD CONSTRAINT [R_27] FOREIGN KEY ([posiljalac]) REFERENCES [Korisnik]([IdK])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [ruta]
	ADD CONSTRAINT [R_28] FOREIGN KEY ([IdA]) REFERENCES [Adresa]([IdA])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [ruta]
	ADD CONSTRAINT [R_38] FOREIGN KEY ([IdK]) REFERENCES [kurir]([IdK])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go


ALTER TABLE [Vozilo]
	ADD CONSTRAINT [R_24] FOREIGN KEY ([IdM]) REFERENCES [LokacijaMagacina]([IdM])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [vozio]
	ADD CONSTRAINT [R_18] FOREIGN KEY ([IdK]) REFERENCES [kurir]([IdK])
		ON DELETE CASCADE
		ON UPDATE CASCADE
go

ALTER TABLE [vozio]
	ADD CONSTRAINT [R_22] FOREIGN KEY ([IdV]) REFERENCES [Vozilo]([IdV])
		ON DELETE CASCADE
		ON UPDATE NO ACTION
go


ALTER TABLE [zahtevi]
	ADD CONSTRAINT [R_21] FOREIGN KEY ([IdK]) REFERENCES [Korisnik]([IdK])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

USE [master]
GO
CREATE LOGIN [kurir] WITH PASSWORD=N'test', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
USE [kurirskaSluzba]
GO
CREATE USER [kurirUser] FOR LOGIN [kurir]
GO
USE [kurirskaSluzba]
GO
GRANT CONTROL ON SCHEMA::dbo TO kurirUser
GO

CREATE TRIGGER VoziloJedinstvenoKurir
   ON  kurir
   AFTER UPDATE
AS 
BEGIN
	DECLARE @kursor CURSOR
	DECLARE @id INT

	SET @kursor = CURSOR FORWARD_ONLY FOR
	SELECT IdV
	FROM INSERTED

	OPEN @kursor

	FETCH NEXT FROM @kursor INTO @id

	WHILE @@fetch_status =0
	BEGIN
		IF (SELECT COUNT(*) FROM dbo.kurir WHERE ISNULL(IdV,-1) = @id)>1 
		BEGIN
			ROLLBACK
			BREAK
		END

		FETCH NEXT FROM @kursor INTO @id
	end

	CLOSE @kursor
	DEALLOCATE @kursor

END
GO

CREATE TRIGGER VozioTabela
   ON  kurir
   AFTER UPDATE
AS 
BEGIN
	DECLARE @kursor CURSOR
	DECLARE @id INT
	DECLARE @idK INT

	SET @kursor = CURSOR FORWARD_ONLY FOR
	SELECT IdV, IdK
	FROM DELETED

	OPEN @kursor

	FETCH NEXT FROM @kursor INTO @id, @IdK

	WHILE @@fetch_status =0
	BEGIN
		IF @id IS NOT NULL 
		BEGIN
			IF NOT EXISTS (SELECT * FROM vozio WHERE IdK=@idK AND IdV=@id)
			BEGIN
				INSERT INTO vozio (IdK, IdV) VALUES (@idK, @id)
			END
		END

		FETCH NEXT FROM @kursor INTO @id, @IdK
	end

	CLOSE @kursor
	DEALLOCATE @kursor

END
GO


CREATE TRIGGER TR_TransportOffer
   ON  Paket
   AFTER INSERT, UPDATE
AS 
BEGIN
	DECLARE @kursor CURSOR
	DECLARE @idP INT
	DECLARE @tip INT
	DECLARE @idPolaska INT
	DECLARE @idDolaska INT
	DECLARE @tezina DECIMAL(10,3)
	DECLARE @xPolaska INT
	DECLARE @yPolaska INT
	DECLARE @xDolaska INT
	DECLARE @yDolaska INT

	SET @kursor = CURSOR FOR
	SELECT IdP, adresa_dolaska, adresa_polaska, tip_paketa, tezina
	FROM inserted

	OPEN @kursor

	FETCH NEXT FROM @kursor INTO @idP, @idDolaska, @idPolaska, @tip, @tezina

	WHILE @@FETCH_STATUS=0
	begin
		SELECT @xDolaska=Xkoordinata, @yDolaska=Ykoordinata
		FROM Adresa
		WHERE IdA=@idDolaska

		SELECT @xPolaska=Xkoordinata, @yPolaska=Ykoordinata
		FROM Adresa
		WHERE IdA=@idPolaska

		UPDATE Paket 
		SET cena=cast(sqrt((power(@xPolaska-@xDolaska,2))+(power(@yPolaska-@yDolaska,2))) as decimal(10,3)) * (
		CASE WHEN @tip = 0 THEN 115
		WHEN @tip = 1 THEN (175+@tezina*100)
		WHEN @tip = 2 THEN (250+@tezina*100)
		WHEN @tip = 3 THEN (350+@tezina*500)
		END)
		WHERE IdP=@idP

		FETCH NEXT FROM @kursor INTO @idP, @idDolaska, @idPolaska, @tip, @tezina
	end


	CLOSE @kursor
	DEALLOCATE @kursor

END
GO

USE [kurirskaSluzba]
GO

EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'
EXEC sp_MSForEachTable 'DELETE FROM ?'
EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'

INSERT INTO [dbo].[Grad]
           ([Naziv],[Postanski_broj])
	VALUES
           ('Grad1',11000),
           ('Grad2',21000),
           ('Grad3',31000),
           ('Grad4',41000),
           ('Grad5',51000),
           ('Grad6',61000)
GO


INSERT INTO [dbo].[Adresa]
           ([Ulica],[Broj],[Xkoordinata],[Ykoordinata],[IdG])
     VALUES
            ('Ulica1',1,5.3,5.3,1),
			('Ulica2',2,4.3,2.3,1),
			('Ulica3',3,3.3,5.0,1),
			('Ulica4',4,9.3,5.9,1),
			('Ulica5',5,15.3,7.5,1),
			('Ulica6',6,10.3,8,1),
			('Ulica7',7,20.3,-10.0,2),
			('Ulica8',8,-5.3,2.2,1),
			('Ulica9',9,0.3,2.9,1),
			('Ulica10',10,-1.3,4.8,1),
			('Ulica11',11,55.3,25.3,3),
			('Ulica12',12,65.3,28.3,3),
			('Ulica13',13,105.3,79.3,4),
			('Ulica14',14,109.3,78.3,4),
			('Ulica15',15,75.3,24.3,3),
			('Ulica16',16,-55.3,-55.3,5),
			('Ulica17',17,-105.3,-68.3,6),
			('Ulica18',18,-102.3,-70.3,6)
GO

INSERT INTO [dbo].[LokacijaMagacina]
           ([IdA])
     VALUES(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),
		(16),(18),(15),(17)
GO

INSERT INTO [dbo].[Korisnik]
           ([ime],[prezime],[korisnicko_ime],[sifra],[adresa])
     VALUES
           ('Ime1','prezime1','Kor1','test',1),
           ('Ime2','prezime2','Kor2','test',2),
           ('Ime3','prezime3','Kor3','test',3),
           ('Ime4','prezime4','Kor4','test',8),
           ('Ime5','prezime5','Kor5','test',9),
           ('Ime6','prezime6','Kor6','test',10),
           ('Ime7','prezime7','Kor7','test',11),
           ('Ime8','prezime8','Kor8','test',15),
           ('Ime9','prezime9','Kor9','test',17),
           ('Ime10','prezime10','Kor10','test',5),
           ('Ime11','prezime11','Kor11','test',1),
           ('Ime12','prezime12','Kor12','test',2),
           ('Ime13','prezime13','Kor13','test',10),
           ('Ime14','prezime14','Kor14','test',7),
           ('Ime15','prezime15','Kor15','test',12),
           ('Ime16','prezime16','Kor16','test',12),
           ('Ime17','prezime17','Kor17','test',2),
           ('Ime18','prezime18','Kor18','test',1),
           ('Ime19','prezime19','Kor19','test',8),
           ('Ime20','prezime20','Kor20','test',6)

GO

INSERT INTO [dbo].[kurir]
           ([IdK],[broj_isporucenih_paketa],[profit],[status],[IdV],[broj_vozacke_dozvole])
     VALUES
           (10,5,10.456,0,NULL,'1'),
           (11,0,0.456,0,NULL,'2'),
		   (12,1,1.456,0,NULL,'3'),
		   (13,6,60.456,0,NULL,'4'),
		   (14,20,110.456,0,NULL,'5'),
		   (15,51,19.456,0,NULL,'6'),
           (16,13,50.456,0,NULL,'7'),
		   (17,9,113.456,0,NULL,'8'),
		   (18,7,1.96,0,NULL,'9')
		   
GO

INSERT INTO [dbo].[Vozilo]
           ([registracioni_broj],[tip_goriva],[potrosnja],[nosivost],[IdM])
     VALUES
           ('456',0,1.2,50.3,1),
		   ('542',1,13.2,10.3,2),
		   ('345',2,5.2,2.3,3),
		   ('44535',0,0.2,5.3,4),
		   ('54345',1,9.2,500.3,5),
		   ('44556',2,1.5,0.3,6),
		   ('4126',0,1.6,18.3,7),
		   ('45879',1,1.8,55.3,8),
		   ('47356',2,4,50.9,9),
		   ('41535',0,3.2,59.3,10),
		   ('54245',1,7.2,45.3,11),
		   ('21456',2,9.2,21.3,12),
		   ('24456',0,3.5,35.3,13),
		   ('4156',1,3.8,12.58,14),
		   ('4506',2,5.1,38.9,15),
		   ('4956',0,0.88,453.78,16),
		   ('4056',1,7.65,1000,17),
		   ('40786',2,4.23,5,18)
GO

INSERT INTO [dbo].[Paket]
           ([tip_paketa],[tezina],[cena],[vreme_kreiranja],[vreme_prihvatanja],
		   [adresa_polaska],[adresa_dolaska],[trenutna_lokacija],[status],[IdV],[posiljalac])
     VALUES
           (0,0.2,5,'20200521 10:34:09 AM',NULL,1,16,NULL,0,NULL,1),
		   (1,50.3,50,'20200522 11:34:19 AM',NULL,2,18,NULL,0,NULL,2),
		   (2,100.22,15,'20200523 12:34:29 AM',NULL,3,18,NULL,0,NULL,1),
		   (3,6.25,25,'20200524 01:34:39 AM',NULL,4,17,NULL,0,NULL,3),
		   (0,2.27,35,'20200525 02:34:49 AM',NULL,5,16,NULL,0,NULL,2),
		   (1,5.35,5.45,'20200526 03:34:59 AM',NULL,6,15,NULL,0,NULL,3),
		   (2,9.21,5.2,'20200527 04:34:12 AM',NULL,7,14,NULL,0,NULL,4),
		   (3,12.96,9,'20200528 05:34:13 AM',NULL,8,13,NULL,0,NULL,5),
		   (0,13.36,13,'20200529 06:34:21 AM',NULL,9,12,NULL,0,NULL,4),
		   (1,20.12,12,'20200121 07:34:10 AM',NULL,10,11,NULL,0,NULL,5),
		   (2,36.87,11,'20200221 08:34:35 AM',NULL,11,10,NULL,0,NULL,1),
		   (3,48.36,87,'20200321 09:34:45 AM',NULL,12,9,NULL,0,NULL,2),
		   (0,9.21,0.5,'20200421 10:24:38 PM',NULL,13,8,NULL,0,NULL,3),
		   (1,7.69,29,'20200621 10:14:02 PM',NULL,14,7,NULL,0,NULL,4),
		   (2,6.15,26,'20200511 10:44:01 PM',NULL,15,6,NULL,0,NULL,5),
		   (3,19.78,23,'20200512 10:54:03 PM',NULL,16,5,NULL,0,NULL,6),
		   (0,18.63,12.85,'20200513 10:31:05 PM',NULL,17,4,NULL,0,NULL,6),
		   (1,6.36,12.5,'20200514 10:32:06 PM',NULL,18,3,NULL,0,NULL,1),
		   (2,2.96,4.98,'20200511 10:33:07 PM',NULL,5,2,NULL,0,NULL,2),
		   (3,1.15,20.47,'20200515 10:38:08 PM',NULL,14,1,NULL,0,NULL,3)
GO


