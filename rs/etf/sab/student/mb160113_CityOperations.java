package rs.etf.sab.student;

import rs.etf.sab.operations.CityOperations;
import rs.etf.sab.student.entiteti.Grad;

import java.util.ArrayList;
import java.util.List;

public class mb160113_CityOperations implements CityOperations {

    @Override
    public int insertCity(String name, String postalCode) {
        try {
            Grad grad = new Grad();
            grad.setNaziv(name);
            grad.setPostanskibroj(postalCode);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(grad);
            JPAClass.em.getTransaction().commit();
            return grad.getIdG();
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public int deleteCity(String... names) {
        try {
            int sum = 0;
            for (String name : names) {
                List<Grad> gradovi = JPAClass.em.createNamedQuery("Grad.findByNaziv", Grad.class).setParameter("naziv", name).getResultList();
                if (gradovi != null && !gradovi.isEmpty()) {
                    JPAClass.em.getTransaction().begin();
                    JPAClass.em.remove(gradovi.get(0));
                    JPAClass.em.getTransaction().commit();
                    sum++;
                }
            }
            return sum;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public boolean deleteCity(int idCity) {
        try {
            Grad grad = JPAClass.em.find(Grad.class, idCity);
            if (grad != null) {
                JPAClass.em.getTransaction().begin();
                JPAClass.em.remove(grad);
                JPAClass.em.getTransaction().commit();
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<Integer> getAllCities() {
        try {
            List<Integer> listaGradova = new ArrayList<>();
            List<Grad> gradovi = JPAClass.em.createNamedQuery("Grad.findAll", Grad.class).getResultList();
            if (gradovi == null) {
                return null;
            }
            gradovi.forEach((grad) -> listaGradova.add(grad.getIdG()));
            return listaGradova;
        } catch (Exception e) {
            return null;
        }
    }

}
