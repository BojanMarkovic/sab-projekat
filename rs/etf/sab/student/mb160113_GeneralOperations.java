package rs.etf.sab.student;

import rs.etf.sab.operations.GeneralOperations;

public class mb160113_GeneralOperations implements GeneralOperations {

    @Override
    public void eraseAll() {
        try {
            JPAClass.em.getTransaction().begin();
            JPAClass.em.createNativeQuery("EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL' ").executeUpdate();
            JPAClass.em.createNativeQuery("EXEC sp_MSForEachTable 'DELETE FROM ?'").executeUpdate();
            JPAClass.em.createNativeQuery("EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'").executeUpdate();
            JPAClass.em.getTransaction().commit();
            JPAClass.createNew();
        } catch (Exception e) {
            return;
        }
    }

}
