/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entiteti;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "Adresa")
@NamedQueries({
    @NamedQuery(name = "Adresa.findAll", query = "SELECT a FROM Adresa a"),
    @NamedQuery(name = "Adresa.findByIdA", query = "SELECT a FROM Adresa a WHERE a.idA = :idA"),
    @NamedQuery(name = "Adresa.findByUlica", query = "SELECT a FROM Adresa a WHERE a.ulica = :ulica"),
    @NamedQuery(name = "Adresa.findByBroj", query = "SELECT a FROM Adresa a WHERE a.broj = :broj"),
    @NamedQuery(name = "Adresa.findByXkoordinata", query = "SELECT a FROM Adresa a WHERE a.xkoordinata = :xkoordinata"),
    @NamedQuery(name = "Adresa.findByYkoordinata", query = "SELECT a FROM Adresa a WHERE a.ykoordinata = :ykoordinata")})
public class Adresa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdA")
    private Integer idA;
    @Basic(optional = false)
    @Column(name = "Ulica")
    private String ulica;
    @Basic(optional = false)
    @Column(name = "Broj")
    private int broj;
    @Basic(optional = false)
    @Column(name = "Xkoordinata")
    private int xkoordinata;
    @Basic(optional = false)
    @Column(name = "Ykoordinata")
    private int ykoordinata;
    @JoinColumn(name = "IdG", referencedColumnName = "IdG")
    @ManyToOne(optional = false)
    private Grad idG;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idA")
    private List<Ruta> rutaList;
    @OneToMany(mappedBy = "adresa")
    private List<Korisnik> korisnikList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresaPolaska")
    private List<Paket> paketList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresaDolaska")
    private List<Paket> paketList1;
    @OneToMany(mappedBy = "trenutnaLokacija")
    private List<Paket> paketList2;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idA")
    private LokacijaMagacina lokacijaMagacina;

    public Adresa() {
    }

    public Adresa(Integer idA) {
        this.idA = idA;
    }

    public Adresa(Integer idA, String ulica, int broj, int xkoordinata, int ykoordinata) {
        this.idA = idA;
        this.ulica = ulica;
        this.broj = broj;
        this.xkoordinata = xkoordinata;
        this.ykoordinata = ykoordinata;
    }

    public Integer getIdA() {
        return idA;
    }

    public void setIdA(Integer idA) {
        this.idA = idA;
    }

    public String getUlica() {
        return ulica;
    }

    public void setUlica(String ulica) {
        this.ulica = ulica;
    }

    public int getBroj() {
        return broj;
    }

    public void setBroj(int broj) {
        this.broj = broj;
    }

    public int getXkoordinata() {
        return xkoordinata;
    }

    public void setXkoordinata(int xkoordinata) {
        this.xkoordinata = xkoordinata;
    }

    public int getYkoordinata() {
        return ykoordinata;
    }

    public void setYkoordinata(int ykoordinata) {
        this.ykoordinata = ykoordinata;
    }

    public Grad getIdG() {
        return idG;
    }

    public void setIdG(Grad idG) {
        this.idG = idG;
    }

    public List<Ruta> getRutaList() {
        return rutaList;
    }

    public void setRutaList(List<Ruta> rutaList) {
        this.rutaList = rutaList;
    }

    public List<Korisnik> getKorisnikList() {
        return korisnikList;
    }

    public void setKorisnikList(List<Korisnik> korisnikList) {
        this.korisnikList = korisnikList;
    }

    public List<Paket> getPaketList() {
        return paketList;
    }

    public void setPaketList(List<Paket> paketList) {
        this.paketList = paketList;
    }

    public List<Paket> getPaketList1() {
        return paketList1;
    }

    public void setPaketList1(List<Paket> paketList1) {
        this.paketList1 = paketList1;
    }

    public List<Paket> getPaketList2() {
        return paketList2;
    }

    public void setPaketList2(List<Paket> paketList2) {
        this.paketList2 = paketList2;
    }

    public LokacijaMagacina getLokacijaMagacina() {
        return lokacijaMagacina;
    }

    public void setLokacijaMagacina(LokacijaMagacina lokacijaMagacina) {
        this.lokacijaMagacina = lokacijaMagacina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idA != null ? idA.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adresa)) {
            return false;
        }
        Adresa other = (Adresa) object;
        if ((this.idA == null && other.idA != null) || (this.idA != null && !this.idA.equals(other.idA))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.etf.sab.student.entiteti.Adresa[ idA=" + idA + " ]";
    }
    
}
