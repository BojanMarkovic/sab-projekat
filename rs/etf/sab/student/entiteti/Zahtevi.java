/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entiteti;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "zahtevi")
@NamedQueries({
    @NamedQuery(name = "Zahtevi.findAll", query = "SELECT z FROM Zahtevi z"),
    @NamedQuery(name = "Zahtevi.findByIdK", query = "SELECT z FROM Zahtevi z WHERE z.idK = :idK"),
    @NamedQuery(name = "Zahtevi.findByBrojVozackeDozvole", query = "SELECT z FROM Zahtevi z WHERE z.brojVozackeDozvole = :brojVozackeDozvole")})
public class Zahtevi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IdK")
    private Integer idK;
    @Basic(optional = false)
    @Column(name = "broj_vozacke_dozvole")
    private String brojVozackeDozvole;
    @JoinColumn(name = "IdK", referencedColumnName = "IdK", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Korisnik korisnik;

    public Zahtevi() {
    }

    public Zahtevi(Integer idK) {
        this.idK = idK;
    }

    public Zahtevi(Integer idK, String brojVozackeDozvole) {
        this.idK = idK;
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public Integer getIdK() {
        return idK;
    }

    public void setIdK(Integer idK) {
        this.idK = idK;
    }

    public String getBrojVozackeDozvole() {
        return brojVozackeDozvole;
    }

    public void setBrojVozackeDozvole(String brojVozackeDozvole) {
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idK != null ? idK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zahtevi)) {
            return false;
        }
        Zahtevi other = (Zahtevi) object;
        if ((this.idK == null && other.idK != null) || (this.idK != null && !this.idK.equals(other.idK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.etf.sab.student.entiteti.Zahtevi[ idK=" + idK + " ]";
    }
    
}
