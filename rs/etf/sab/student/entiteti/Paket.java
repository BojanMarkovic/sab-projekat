/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entiteti;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "Paket")
@NamedQueries({
    @NamedQuery(name = "Paket.findAll", query = "SELECT p FROM Paket p"),
    @NamedQuery(name = "Paket.findByIdP", query = "SELECT p FROM Paket p WHERE p.idP = :idP"),
    @NamedQuery(name = "Paket.findByTipPaketa", query = "SELECT p FROM Paket p WHERE p.tipPaketa = :tipPaketa"),
    @NamedQuery(name = "Paket.findByTezina", query = "SELECT p FROM Paket p WHERE p.tezina = :tezina"),
    @NamedQuery(name = "Paket.findByCena", query = "SELECT p FROM Paket p WHERE p.cena = :cena"),
    @NamedQuery(name = "Paket.findByVremeKreiranja", query = "SELECT p FROM Paket p WHERE p.vremeKreiranja = :vremeKreiranja"),
    @NamedQuery(name = "Paket.findByVremePrihvatanja", query = "SELECT p FROM Paket p WHERE p.vremePrihvatanja = :vremePrihvatanja"),
    @NamedQuery(name = "Paket.findByStatus", query = "SELECT p FROM Paket p WHERE p.status = :status"),
    @NamedQuery(name = "Paket.findByPotrebaZaDostavom", query = "SELECT p FROM Paket p WHERE p.potrebaZaDostavom = :potrebaZaDostavom")})
public class Paket implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdP")
    private Integer idP;
    @Basic(optional = false)
    @Column(name = "tip_paketa")
    private int tipPaketa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "tezina")
    private BigDecimal tezina;
    @Column(name = "cena")
    private BigDecimal cena;
    @Basic(optional = false)
    @Column(name = "vreme_kreiranja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vremeKreiranja;
    @Column(name = "vreme_prihvatanja")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vremePrihvatanja;
    @Column(name = "status")
    private Integer status;
    @Column(name = "potreba_za_dostavom")
    private Integer potrebaZaDostavom;
    @JoinColumn(name = "adresa_polaska", referencedColumnName = "IdA")
    @ManyToOne(optional = false)
    private Adresa adresaPolaska;
    @JoinColumn(name = "adresa_dolaska", referencedColumnName = "IdA")
    @ManyToOne(optional = false)
    private Adresa adresaDolaska;
    @JoinColumn(name = "trenutna_lokacija", referencedColumnName = "IdA")
    @ManyToOne
    private Adresa trenutnaLokacija;
    @JoinColumn(name = "posiljalac", referencedColumnName = "IdK")
    @ManyToOne(optional = false)
    private Korisnik posiljalac;
    @JoinColumn(name = "IdV", referencedColumnName = "IdV")
    @ManyToOne
    private Vozilo idV;

    public Paket() {
    }

    public Paket(Integer idP) {
        this.idP = idP;
    }

    public Paket(Integer idP, int tipPaketa, BigDecimal tezina, Date vremeKreiranja) {
        this.idP = idP;
        this.tipPaketa = tipPaketa;
        this.tezina = tezina;
        this.vremeKreiranja = vremeKreiranja;
    }

    public Integer getIdP() {
        return idP;
    }

    public void setIdP(Integer idP) {
        this.idP = idP;
    }

    public int getTipPaketa() {
        return tipPaketa;
    }

    public void setTipPaketa(int tipPaketa) {
        this.tipPaketa = tipPaketa;
    }

    public BigDecimal getTezina() {
        return tezina;
    }

    public void setTezina(BigDecimal tezina) {
        this.tezina = tezina;
    }

    public BigDecimal getCena() {
        return cena;
    }

    public void setCena(BigDecimal cena) {
        this.cena = cena;
    }

    public Date getVremeKreiranja() {
        return vremeKreiranja;
    }

    public void setVremeKreiranja(Date vremeKreiranja) {
        this.vremeKreiranja = vremeKreiranja;
    }

    public Date getVremePrihvatanja() {
        return vremePrihvatanja;
    }

    public void setVremePrihvatanja(Date vremePrihvatanja) {
        this.vremePrihvatanja = vremePrihvatanja;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPotrebaZaDostavom() {
        return potrebaZaDostavom;
    }

    public void setPotrebaZaDostavom(Integer potrebaZaDostavom) {
        this.potrebaZaDostavom = potrebaZaDostavom;
    }

    public Adresa getAdresaPolaska() {
        return adresaPolaska;
    }

    public void setAdresaPolaska(Adresa adresaPolaska) {
        this.adresaPolaska = adresaPolaska;
    }

    public Adresa getAdresaDolaska() {
        return adresaDolaska;
    }

    public void setAdresaDolaska(Adresa adresaDolaska) {
        this.adresaDolaska = adresaDolaska;
    }

    public Adresa getTrenutnaLokacija() {
        return trenutnaLokacija;
    }

    public void setTrenutnaLokacija(Adresa trenutnaLokacija) {
        this.trenutnaLokacija = trenutnaLokacija;
    }

    public Korisnik getPosiljalac() {
        return posiljalac;
    }

    public void setPosiljalac(Korisnik posiljalac) {
        this.posiljalac = posiljalac;
    }

    public Vozilo getIdV() {
        return idV;
    }

    public void setIdV(Vozilo idV) {
        this.idV = idV;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idP != null ? idP.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paket)) {
            return false;
        }
        Paket other = (Paket) object;
        if ((this.idP == null && other.idP != null) || (this.idP != null && !this.idP.equals(other.idP))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.etf.sab.student.entiteti.Paket[ idP=" + idP + " ]";
    }
    
}
