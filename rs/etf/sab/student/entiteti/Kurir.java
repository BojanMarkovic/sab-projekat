/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entiteti;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "kurir")
@NamedQueries({
    @NamedQuery(name = "Kurir.findAll", query = "SELECT k FROM Kurir k"),
    @NamedQuery(name = "Kurir.findByIdK", query = "SELECT k FROM Kurir k WHERE k.idK = :idK"),
    @NamedQuery(name = "Kurir.findByBrojIsporucenihPaketa", query = "SELECT k FROM Kurir k WHERE k.brojIsporucenihPaketa = :brojIsporucenihPaketa"),
    @NamedQuery(name = "Kurir.findByProfit", query = "SELECT k FROM Kurir k WHERE k.profit = :profit"),
    @NamedQuery(name = "Kurir.findByStatus", query = "SELECT k FROM Kurir k WHERE k.status = :status"),
    @NamedQuery(name = "Kurir.findByBrojVozackeDozvole", query = "SELECT k FROM Kurir k WHERE k.brojVozackeDozvole = :brojVozackeDozvole"),
    @NamedQuery(name = "Kurir.findByTempProfit", query = "SELECT k FROM Kurir k WHERE k.tempProfit = :tempProfit")})
public class Kurir implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "IdK")
    private Integer idK;
    @Column(name = "broj_isporucenih_paketa")
    private Integer brojIsporucenihPaketa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "profit")
    private BigDecimal profit;
    @Column(name = "status")
    private Integer status;
    @Basic(optional = false)
    @Column(name = "broj_vozacke_dozvole")
    private String brojVozackeDozvole;
    @Column(name = "tempProfit")
    private BigDecimal tempProfit;
    @JoinTable(name = "vozio", joinColumns = {
        @JoinColumn(name = "IdK", referencedColumnName = "IdK")}, inverseJoinColumns = {
        @JoinColumn(name = "IdV", referencedColumnName = "IdV")})
    @ManyToMany
    private List<Vozilo> voziloList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idK")
    private List<Ruta> rutaList;
    @JoinColumn(name = "IdK", referencedColumnName = "IdK", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Korisnik korisnik;
    @JoinColumn(name = "IdV", referencedColumnName = "IdV")
    @ManyToOne
    private Vozilo idV;

    public Kurir() {
    }

    public Kurir(Integer idK) {
        this.idK = idK;
    }

    public Kurir(Integer idK, String brojVozackeDozvole) {
        this.idK = idK;
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public Integer getIdK() {
        return idK;
    }

    public void setIdK(Integer idK) {
        this.idK = idK;
    }

    public Integer getBrojIsporucenihPaketa() {
        return brojIsporucenihPaketa;
    }

    public void setBrojIsporucenihPaketa(Integer brojIsporucenihPaketa) {
        this.brojIsporucenihPaketa = brojIsporucenihPaketa;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBrojVozackeDozvole() {
        return brojVozackeDozvole;
    }

    public void setBrojVozackeDozvole(String brojVozackeDozvole) {
        this.brojVozackeDozvole = brojVozackeDozvole;
    }

    public BigDecimal getTempProfit() {
        return tempProfit;
    }

    public void setTempProfit(BigDecimal tempProfit) {
        this.tempProfit = tempProfit;
    }

    public List<Vozilo> getVoziloList() {
        return voziloList;
    }

    public void setVoziloList(List<Vozilo> voziloList) {
        this.voziloList = voziloList;
    }

    public List<Ruta> getRutaList() {
        return rutaList;
    }

    public void setRutaList(List<Ruta> rutaList) {
        this.rutaList = rutaList;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Vozilo getIdV() {
        return idV;
    }

    public void setIdV(Vozilo idV) {
        this.idV = idV;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idK != null ? idK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Kurir)) {
            return false;
        }
        Kurir other = (Kurir) object;
        if ((this.idK == null && other.idK != null) || (this.idK != null && !this.idK.equals(other.idK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.etf.sab.student.entiteti.Kurir[ idK=" + idK + " ]";
    }
    
}
