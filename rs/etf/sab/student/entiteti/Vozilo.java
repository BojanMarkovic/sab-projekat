/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entiteti;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "Vozilo")
@NamedQueries({
    @NamedQuery(name = "Vozilo.findAll", query = "SELECT v FROM Vozilo v"),
    @NamedQuery(name = "Vozilo.findByRegistracioniBroj", query = "SELECT v FROM Vozilo v WHERE v.registracioniBroj = :registracioniBroj"),
    @NamedQuery(name = "Vozilo.findByTipGoriva", query = "SELECT v FROM Vozilo v WHERE v.tipGoriva = :tipGoriva"),
    @NamedQuery(name = "Vozilo.findByPotrosnja", query = "SELECT v FROM Vozilo v WHERE v.potrosnja = :potrosnja"),
    @NamedQuery(name = "Vozilo.findByNosivost", query = "SELECT v FROM Vozilo v WHERE v.nosivost = :nosivost"),
    @NamedQuery(name = "Vozilo.findByIdV", query = "SELECT v FROM Vozilo v WHERE v.idV = :idV"),
    @NamedQuery(name = "Vozilo.findByZauzet", query = "SELECT v FROM Vozilo v WHERE v.zauzet = :zauzet")})
public class Vozilo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "registracioni_broj")
    private String registracioniBroj;
    @Basic(optional = false)
    @Column(name = "tip_goriva")
    private int tipGoriva;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "potrosnja")
    private BigDecimal potrosnja;
    @Basic(optional = false)
    @Column(name = "nosivost")
    private BigDecimal nosivost;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdV")
    private Integer idV;
    @Column(name = "zauzet")
    private BigDecimal zauzet;
    @ManyToMany(mappedBy = "voziloList")
    private List<Kurir> kurirList;
    @OneToMany(mappedBy = "idV")
    private List<Kurir> kurirList1;
    @OneToMany(mappedBy = "idV")
    private List<Paket> paketList;
    @JoinColumn(name = "IdM", referencedColumnName = "IdM")
    @ManyToOne
    private LokacijaMagacina idM;

    public Vozilo() {
    }

    public Vozilo(Integer idV) {
        this.idV = idV;
    }

    public Vozilo(Integer idV, String registracioniBroj, int tipGoriva, BigDecimal potrosnja, BigDecimal nosivost) {
        this.idV = idV;
        this.registracioniBroj = registracioniBroj;
        this.tipGoriva = tipGoriva;
        this.potrosnja = potrosnja;
        this.nosivost = nosivost;
    }

    public String getRegistracioniBroj() {
        return registracioniBroj;
    }

    public void setRegistracioniBroj(String registracioniBroj) {
        this.registracioniBroj = registracioniBroj;
    }

    public int getTipGoriva() {
        return tipGoriva;
    }

    public void setTipGoriva(int tipGoriva) {
        this.tipGoriva = tipGoriva;
    }

    public BigDecimal getPotrosnja() {
        return potrosnja;
    }

    public void setPotrosnja(BigDecimal potrosnja) {
        this.potrosnja = potrosnja;
    }

    public BigDecimal getNosivost() {
        return nosivost;
    }

    public void setNosivost(BigDecimal nosivost) {
        this.nosivost = nosivost;
    }

    public Integer getIdV() {
        return idV;
    }

    public void setIdV(Integer idV) {
        this.idV = idV;
    }

    public BigDecimal getZauzet() {
        return zauzet;
    }

    public void setZauzet(BigDecimal zauzet) {
        this.zauzet = zauzet;
    }

    public List<Kurir> getKurirList() {
        return kurirList;
    }

    public void setKurirList(List<Kurir> kurirList) {
        this.kurirList = kurirList;
    }

    public List<Kurir> getKurirList1() {
        return kurirList1;
    }

    public void setKurirList1(List<Kurir> kurirList1) {
        this.kurirList1 = kurirList1;
    }

    public List<Paket> getPaketList() {
        return paketList;
    }

    public void setPaketList(List<Paket> paketList) {
        this.paketList = paketList;
    }

    public LokacijaMagacina getIdM() {
        return idM;
    }

    public void setIdM(LokacijaMagacina idM) {
        this.idM = idM;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idV != null ? idV.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vozilo)) {
            return false;
        }
        Vozilo other = (Vozilo) object;
        if ((this.idV == null && other.idV != null) || (this.idV != null && !this.idV.equals(other.idV))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.etf.sab.student.entiteti.Vozilo[ idV=" + idV + " ]";
    }
    
}
