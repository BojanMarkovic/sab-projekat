/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.etf.sab.student.entiteti;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "LokacijaMagacina")
@NamedQueries({
    @NamedQuery(name = "LokacijaMagacina.findAll", query = "SELECT l FROM LokacijaMagacina l"),
    @NamedQuery(name = "LokacijaMagacina.findByIdM", query = "SELECT l FROM LokacijaMagacina l WHERE l.idM = :idM")})
public class LokacijaMagacina implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IdM")
    private Integer idM;
    @JoinColumn(name = "IdA", referencedColumnName = "IdA")
    @OneToOne(optional = false)
    private Adresa idA;
    @OneToMany(mappedBy = "idM")
    private List<Vozilo> voziloList;

    public LokacijaMagacina() {
    }

    public LokacijaMagacina(Integer idM) {
        this.idM = idM;
    }

    public Integer getIdM() {
        return idM;
    }

    public void setIdM(Integer idM) {
        this.idM = idM;
    }

    public Adresa getIdA() {
        return idA;
    }

    public void setIdA(Adresa idA) {
        this.idA = idA;
    }

    public List<Vozilo> getVoziloList() {
        return voziloList;
    }

    public void setVoziloList(List<Vozilo> voziloList) {
        this.voziloList = voziloList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idM != null ? idM.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LokacijaMagacina)) {
            return false;
        }
        LokacijaMagacina other = (LokacijaMagacina) object;
        if ((this.idM == null && other.idM != null) || (this.idM != null && !this.idM.equals(other.idM))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "rs.etf.sab.student.entiteti.LokacijaMagacina[ idM=" + idM + " ]";
    }
    
}
