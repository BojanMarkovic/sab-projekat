package rs.etf.sab.student;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAClass {
    public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("kurirskaSluzbaPU");
    public static EntityManager em = emf.createEntityManager();

    static void createNew() {
        emf = Persistence.createEntityManagerFactory("kurirskaSluzbaPU");
        em = emf.createEntityManager();
    }
}
