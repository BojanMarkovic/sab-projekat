package rs.etf.sab.student;

import rs.etf.sab.operations.VehicleOperations;
import rs.etf.sab.student.entiteti.Kurir;
import rs.etf.sab.student.entiteti.LokacijaMagacina;
import rs.etf.sab.student.entiteti.Paket;
import rs.etf.sab.student.entiteti.Vozilo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class mb160113_VehicleOperations implements VehicleOperations {

    @Override
    public boolean insertVehicle(String licencePlateNumber, int fuelType, BigDecimal fuelConsumtion, BigDecimal capacity) {
        try {
            Vozilo vozilo = new Vozilo();
            vozilo.setRegistracioniBroj(licencePlateNumber);
            vozilo.setTipGoriva(fuelType);
            vozilo.setPotrosnja(fuelConsumtion);
            vozilo.setNosivost(capacity);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(vozilo);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int deleteVehicles(String... licencePlateNumbers) {
        try {
            int sum = 0;
            for (String licencePlate : licencePlateNumbers) {
                List<Vozilo> vozila = JPAClass.em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licencePlate).getResultList();
                if (vozila != null && !vozila.isEmpty()) {
                    try {
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.remove(vozila.get(0));
                        JPAClass.em.getTransaction().commit();
                    } catch (Exception e) {
                    }
                    sum++;
                }
            }
            return sum;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public List<String> getAllVehichles() {
        try {
            List<Vozilo> vozila = JPAClass.em.createNamedQuery("Vozilo.findAll", Vozilo.class).getResultList();
            if (vozila == null) {
                return null;
            }
            List<String> registarskiBrojevi = new ArrayList<>();
            vozila.forEach(vozilo -> registarskiBrojevi.add(vozilo.getRegistracioniBroj()));
            return registarskiBrojevi;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean changeFuelType(String licensePlateNumber, int fuelType) {
        try {
            Vozilo vozilo = JPAClass.em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licensePlateNumber).getSingleResult();
            if (vozilo == null) {
                return false;
            }
            if (vozilo.getIdM() == null) {
                return false;
            }
            vozilo.setTipGoriva(fuelType);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(vozilo);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean changeConsumption(String licensePlateNumber, BigDecimal fuelConsumption) {
        try {
            Vozilo vozilo = JPAClass.em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licensePlateNumber).getSingleResult();
            if (vozilo == null) {
                return false;
            }
            if (vozilo.getIdM() == null) {
                return false;
            }
            vozilo.setPotrosnja(fuelConsumption);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(vozilo);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean changeCapacity(String licensePlateNumber, BigDecimal capacity) {
        try {
            Vozilo vozilo = JPAClass.em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licensePlateNumber).getSingleResult();
            if (vozilo == null) {
                return false;
            }
            if (vozilo.getIdM() == null) {
                return false;
            }
            vozilo.setNosivost(capacity);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(vozilo);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean parkVehicle(String licencePlateNumbers, int idStockroom) {
        try {
            LokacijaMagacina magacin = JPAClass.em.find(LokacijaMagacina.class, idStockroom);
            if (magacin == null) {
                return false;
            }
            Vozilo vozilo = JPAClass.em.createNamedQuery("Vozilo.findByRegistracioniBroj", Vozilo.class).setParameter("registracioniBroj", licencePlateNumbers).getSingleResult();
            List<Paket> listaPaketa = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE p.idV.idV = :idV", Paket.class).setParameter("idV", vozilo.getIdV()).getResultList();
            if (listaPaketa == null || listaPaketa.isEmpty()) {
                List<Kurir> listaKurira = JPAClass.em.createQuery("SELECT k FROM Kurir k WHERE k.idV.idV = :idV AND k.status=0", Kurir.class).setParameter("idV", vozilo.getIdV()).getResultList();
                if (listaKurira == null || listaKurira.isEmpty()) {
                    vozilo.setIdM(magacin);
                    JPAClass.em.getTransaction().begin();
                    JPAClass.em.persist(vozilo);
                    JPAClass.em.getTransaction().commit();
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

}
