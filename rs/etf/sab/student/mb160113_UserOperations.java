package rs.etf.sab.student;

import rs.etf.sab.operations.UserOperations;
import rs.etf.sab.student.entiteti.Administrator;
import rs.etf.sab.student.entiteti.Adresa;
import rs.etf.sab.student.entiteti.Korisnik;
import rs.etf.sab.student.entiteti.Paket;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class mb160113_UserOperations implements UserOperations {

    private static final String PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]).{8,100})";

    @Override
    public boolean declareAdmin(String userName) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getSingleResult();
            if (korisnik != null) {
                List<Administrator> admini = JPAClass.em.createNamedQuery("Administrator.findByIdK", Administrator.class).setParameter("idK", korisnik.getIdK()).getResultList();
                if (admini != null && !admini.isEmpty()) {
                    return false;
                }
                Administrator administrator = new Administrator();
                administrator.setIdK(korisnik.getIdK());
                administrator.setKorisnik(korisnik);
                korisnik.setAdministrator(administrator);
                JPAClass.em.getTransaction().begin();
                JPAClass.em.persist(administrator);
                JPAClass.em.getTransaction().commit();
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int getSentPackages(String... userNames) {
        try {
            int sum = 0;
            boolean flag = true;
            for (String name : userNames) {
                List<Korisnik> korisnici = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", name).getResultList();
                if (korisnici == null || korisnici.isEmpty()) {
                    continue;
                }
                flag = false;
                List<Paket> paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE p.posiljalac = :korisnik", Paket.class).setParameter("korisnik", korisnici.get(0)).getResultList();
                if (paketi == null || paketi.isEmpty()) {
                    continue;
                }
                sum += paketi.size();
            }
            if (flag) {
                return -1;
            } else {
                return sum;
            }
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public int deleteUsers(String... userNames) {
        try {
            int sum = 0;
            for (String userName : userNames) {
                List<Korisnik> korisnici = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getResultList();
                if (korisnici == null || korisnici.isEmpty()) {
                    continue;
                }
                try {
                    JPAClass.em.getTransaction().begin();
                    JPAClass.em.remove(korisnici.get(0));
                    JPAClass.em.getTransaction().commit();
                } catch (Exception e) {
                }
                sum++;
            }
            return sum;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public List<String> getAllUsers() {
        try {
            List<Korisnik> korisnici = JPAClass.em.createNamedQuery("Korisnik.findAll", Korisnik.class).getResultList();
            if (korisnici == null) {
                return null;
            }
            List<String> imenaKorisnika = new ArrayList<>();
            korisnici.forEach(korisnik -> imenaKorisnika.add(korisnik.getKorisnickoIme()));
            return imenaKorisnika;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean insertUser(String userName, String firstName, String lastName, String password, int idAddress) {
        try {
            if (!Character.isUpperCase(firstName.charAt(0))) {
                return false;
            }
            if (!Character.isUpperCase(lastName.charAt(0))) {
                return false;
            }
            Pattern pattern = Pattern.compile(PATTERN);
            if (!pattern.matcher(password).matches()) {
                return false;
            }
            Adresa adresa = JPAClass.em.find(Adresa.class, idAddress);
            if (adresa == null) {
                return false;
            }
            Korisnik korisnik = new Korisnik();
            korisnik.setIme(firstName);
            korisnik.setPrezime(lastName);
            korisnik.setSifra(password);
            korisnik.setKorisnickoIme(userName);
            korisnik.setAdresa(adresa);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(korisnik);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
