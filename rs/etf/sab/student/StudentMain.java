package rs.etf.sab.student;

import rs.etf.sab.operations.*;
import rs.etf.sab.tests.TestHandler;
import rs.etf.sab.tests.TestRunner;


public class StudentMain {

    public static void main(String[] args) {
        AddressOperations addressOperations = new mb160113_AddressOperations(); // Change this to your implementation.
        CityOperations cityOperations = new mb160113_CityOperations(); // Do it for all classes.
        CourierOperations courierOperations = new mb160113_CourierOperations(); // e.g. = new MyDistrictOperations();
        CourierRequestOperation courierRequestOperation = new mb160113_CourierRequestOperation();
        DriveOperation driveOperation = new mb160113_DriveOperation();
        GeneralOperations generalOperations = new mb160113_GeneralOperations();
        PackageOperations packageOperations = new mb160113_PackageOperations();
        StockroomOperations stockroomOperations = new mb160113_StockroomOperations();
        UserOperations userOperations = new mb160113_UserOperations();
        VehicleOperations vehicleOperations = new mb160113_VehicleOperations();


        TestHandler.createInstance(
                addressOperations,
                cityOperations,
                courierOperations,
                courierRequestOperation,
                driveOperation,
                generalOperations,
                packageOperations,
                stockroomOperations,
                userOperations,
                vehicleOperations);

        TestRunner.runTests();
    }
}
