package rs.etf.sab.student;

import rs.etf.sab.operations.StockroomOperations;
import rs.etf.sab.student.entiteti.Adresa;
import rs.etf.sab.student.entiteti.LokacijaMagacina;
import rs.etf.sab.student.entiteti.Paket;
import rs.etf.sab.student.entiteti.Vozilo;

import java.util.ArrayList;
import java.util.List;

public class mb160113_StockroomOperations implements StockroomOperations {

    private boolean checkIfEmpty(int id, int adresaId) {
        List<Vozilo> vozila = JPAClass.em.createQuery("SELECT v FROM Vozilo v WHERE v.idM.idM = :idM", Vozilo.class).setParameter("idM", id).getResultList();
        if (vozila == null) {
            return false;
        }
        if (!vozila.isEmpty()) {
            return false;
        }
        List<Paket> paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE (p.trenutnaLokacija.idA = :idTA) OR (p.adresaPolaska.idA = :idPa AND p.status=1)", Paket.class)
                .setParameter("idTA", adresaId).setParameter("idPa", adresaId).getResultList();
        if (paketi == null) {
            return false;
        }
        if (!paketi.isEmpty()) {
            return false;
        }
        return true;
    }

    @Override
    public int deleteStockroomFromCity(int idCity) {
        try {
            LokacijaMagacina magacin = JPAClass.em.createQuery("SELECT a FROM LokacijaMagacina a WHERE a.idA.idG.idG = :grad", LokacijaMagacina.class)
                    .setParameter("grad", idCity).getSingleResult();
            if (magacin == null) {
                return -1;
            }
            if (!checkIfEmpty(magacin.getIdM(), magacin.getIdA().getIdA())) {
                return -1;
            }
            int id = magacin.getIdM();
            JPAClass.em.getTransaction().begin();
            JPAClass.em.remove(magacin);
            JPAClass.em.getTransaction().commit();
            return id;
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public List<Integer> getAllStockrooms() {
        try {
            List<LokacijaMagacina> magacini = JPAClass.em.createNamedQuery("LokacijaMagacina.findAll", LokacijaMagacina.class).getResultList();
            if (magacini == null) {
                return null;
            }
            List<Integer> kljuceviMagacina = new ArrayList<>();
            magacini.forEach(magacin -> kljuceviMagacina.add(magacin.getIdM()));
            return kljuceviMagacina;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int insertStockroom(int address) {
        try {
            Adresa adresa = JPAClass.em.find(Adresa.class, address);
            if (adresa == null) {
                return -1;
            }
            List<LokacijaMagacina> magacini = JPAClass.em.createNamedQuery("LokacijaMagacina.findAll", LokacijaMagacina.class).getResultList();
            if (magacini != null) {
                for (LokacijaMagacina magacin : magacini) {
                    if (magacin.getIdA().getIdG().equals(adresa.getIdG())) {
                        return -1;
                    }
                }
            }
            LokacijaMagacina lokacijaMagacina = new LokacijaMagacina();
            lokacijaMagacina.setIdA(adresa);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(lokacijaMagacina);
            JPAClass.em.getTransaction().commit();
            return lokacijaMagacina.getIdM();
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public boolean deleteStockroom(int idStockroom) {
        try {
            LokacijaMagacina lokacijaMagacina = JPAClass.em.find(LokacijaMagacina.class, idStockroom);
            if (lokacijaMagacina == null) {
                return false;
            }
            if (!checkIfEmpty(lokacijaMagacina.getIdM(), lokacijaMagacina.getIdA().getIdA())) {
                return false;
            }
            JPAClass.em.getTransaction().begin();
            JPAClass.em.remove(lokacijaMagacina);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
