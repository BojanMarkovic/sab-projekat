package rs.etf.sab.student;

import rs.etf.sab.operations.AddressOperations;
import rs.etf.sab.student.entiteti.Adresa;
import rs.etf.sab.student.entiteti.Grad;

import java.util.ArrayList;
import java.util.List;

public class mb160113_AddressOperations implements AddressOperations {

    @Override
    public int deleteAllAddressesFromCity(int idCity) {
        try {
            int sum = 0;
            List<Adresa> adrese = JPAClass.em.createQuery("SELECT a FROM Adresa a WHERE a.idG.idG = :grad", Adresa.class).setParameter("grad", idCity).getResultList();
            if (adrese != null) {
                for (Adresa adresa : adrese) {
                    try {
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.remove(adresa);
                        JPAClass.em.getTransaction().commit();
                        sum++;
                    } catch (Exception e) {
                    }
                }
            }
            return sum;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public int insertAddress(String street, int number, int cityId, int xCord, int yCord) {
        try {
            Grad grad = JPAClass.em.find(Grad.class, cityId);
            if (grad != null) {
                Adresa adresa = new Adresa();
                adresa.setBroj(number);
                adresa.setUlica(street);
                adresa.setIdG(grad);
                adresa.setXkoordinata(xCord);
                adresa.setYkoordinata(yCord);
                JPAClass.em.getTransaction().begin();
                JPAClass.em.persist(adresa);
                JPAClass.em.getTransaction().commit();
                return adresa.getIdA();
            }
            return -1;
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public int deleteAddresses(String name, int number) {
        try {
            int sum = 0;
            List<Adresa> adrese = JPAClass.em.createQuery("SELECT a FROM Adresa a WHERE a.broj = :broj AND a.ulica = :ulica", Adresa.class)
                    .setParameter("broj", number).setParameter("ulica", name).getResultList();
            if (adrese != null) {
                for (Adresa adresa : adrese) {
                    try {
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.remove(adresa);
                        JPAClass.em.getTransaction().commit();
                        sum++;
                    } catch (Exception e) {
                    }
                }
            }
            return sum;
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public boolean deleteAdress(int idAddress) {
        try {
            Adresa adresa = JPAClass.em.find(Adresa.class, idAddress);
            if (adresa != null) {
                JPAClass.em.getTransaction().begin();
                JPAClass.em.remove(adresa);
                JPAClass.em.getTransaction().commit();
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<Integer> getAllAddresses() {
        try {
            List<Adresa> adrese = JPAClass.em.createNamedQuery("Adresa.findAll", Adresa.class).getResultList();
            if (adrese == null) {
                return null;
            }
            List<Integer> kljuceviAdresa = new ArrayList<>();
            adrese.forEach(adresa -> kljuceviAdresa.add(adresa.getIdA()));
            return kljuceviAdresa;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Integer> getAllAddressesFromCity(int idCity) {
        try {
            List<Adresa> adrese = JPAClass.em.createQuery("SELECT a FROM Adresa a WHERE a.idG.idG = :grad", Adresa.class).setParameter("grad", idCity).getResultList();
            if (adrese == null || adrese.isEmpty()) {//cudna stvar
                return null;
            }
            List<Integer> kljuceviAdresa = new ArrayList<>();
            adrese.forEach(adresa -> kljuceviAdresa.add(adresa.getIdA()));
            return kljuceviAdresa;
        } catch (Exception e) {
            return null;
        }
    }

}
