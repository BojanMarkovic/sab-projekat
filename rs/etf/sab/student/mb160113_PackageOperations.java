package rs.etf.sab.student;

import rs.etf.sab.operations.PackageOperations;
import rs.etf.sab.student.entiteti.Adresa;
import rs.etf.sab.student.entiteti.Korisnik;
import rs.etf.sab.student.entiteti.Paket;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import rs.etf.sab.student.entiteti.LokacijaMagacina;

public class mb160113_PackageOperations implements PackageOperations {

    @Override
    public int insertPackage(int addressFrom, int addressTo, String userName, int packageType, BigDecimal weight) {
        try {
            Adresa adresaOd = JPAClass.em.createNamedQuery("Adresa.findByIdA", Adresa.class).setParameter("idA", addressFrom).getSingleResult();
            Adresa adresaDo = JPAClass.em.createNamedQuery("Adresa.findByIdA", Adresa.class).setParameter("idA", addressTo).getSingleResult();
            if (adresaOd == null || adresaDo == null) {
                return -1;
            }
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getSingleResult();
            if (korisnik == null) {
                return -1;
            }
            Paket paket = new Paket();
            paket.setAdresaDolaska(adresaDo);
            paket.setAdresaPolaska(adresaOd);
            paket.setTezina(weight);
            paket.setPosiljalac(korisnik);
            paket.setTipPaketa(packageType);
            paket.setStatus(0);
            paket.setCena(BigDecimal.ZERO);
            paket.setVremeKreiranja(new java.util.Date(System.currentTimeMillis()));
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(paket);
            JPAClass.em.getTransaction().commit();
            return paket.getIdP();
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public boolean acceptAnOffer(int offerId) {
        try {
            Paket zahtev = JPAClass.em.find(Paket.class, offerId);
            if (zahtev == null || zahtev.getStatus() != 0) {
                return false;
            }
            zahtev.setStatus(1);
            zahtev.setVremePrihvatanja(new java.util.Date(System.currentTimeMillis()));
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(zahtev);
            JPAClass.em.getTransaction().commit();
            JPAClass.em.refresh(zahtev);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public boolean rejectAnOffer(int offerId) {
        try {
            Paket zahtev = JPAClass.em.find(Paket.class, offerId);
            if (zahtev == null || zahtev.getStatus() != 0) {
                return false;
            }
            zahtev.setStatus(4);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(zahtev);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<Integer> getAllPackages() {
        try {
            List<Paket> paketi = JPAClass.em.createNamedQuery("Paket.findAll", Paket.class).getResultList();
            if (paketi == null) {
                return null;
            }
            List<Integer> listaPaketa = new ArrayList<>();
            paketi.forEach(paket -> listaPaketa.add(paket.getIdP()));
            return listaPaketa;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Integer> getAllPackagesWithSpecificType(int type) {
        try {
            List<Paket> paketi = JPAClass.em.createNamedQuery("Paket.findByTipPaketa", Paket.class).setParameter("tipPaketa", type).getResultList();
            if (paketi == null) {
                return null;
            }
            List<Integer> listaPaketa = new ArrayList<>();
            paketi.forEach(paket -> listaPaketa.add(paket.getIdP()));
            return listaPaketa;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Integer> getAllUndeliveredPackages() {
        try {
            List<Paket> paketi = JPAClass.em.createNamedQuery("Paket.findAll", Paket.class).getResultList();
            if (paketi == null) {
                return null;
            }
            List<Integer> listaPaketa = new ArrayList<>();
            paketi.forEach(paket -> {
                if ((paket.getStatus() == 1) || (paket.getStatus() == 2)) {
                    listaPaketa.add(paket.getIdP());
                }
            });
            return listaPaketa;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Integer> getAllUndeliveredPackagesFromCity(int cityId) {
        try {
            List<Paket> paketi = JPAClass.em.createNamedQuery("Paket.findAll", Paket.class).getResultList();
            if (paketi == null) {
                return null;
            }
            List<Integer> listaPaketa = new ArrayList<>();
            paketi.forEach(paket -> {
                if (((paket.getStatus() == 1) || (paket.getStatus() == 2)) && (paket.getAdresaPolaska().getIdG().getIdG() == cityId)) {
                    listaPaketa.add(paket.getIdP());
                }
            });
            return listaPaketa;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Integer> getAllPackagesCurrentlyAtCity(int cityId) {
        try {
            List<Paket> paketi = JPAClass.em.createNamedQuery("Paket.findAll", Paket.class).getResultList();
            if (paketi == null) {
                return null;
            }
            List<Integer> listaPaketa = new ArrayList<>();
            paketi.forEach(paket -> {
                if (paket.getIdV() == null) {
                    if (paket.getTrenutnaLokacija() != null) {
                        if (paket.getTrenutnaLokacija() == paket.getAdresaDolaska()) {
                            if (paket.getTrenutnaLokacija().getIdG().getIdG() == cityId) {
                                listaPaketa.add(paket.getIdP());
                            }
                        } else {
                            List<LokacijaMagacina> magacini = JPAClass.em.createQuery("SELECT l FROM LokacijaMagacina l WHERE l.idA.idA = :adresa", LokacijaMagacina.class)
                                    .setParameter("adresa", paket.getTrenutnaLokacija().getIdA()).getResultList();
                            if (magacini != null) {
                                if (!magacini.isEmpty()) {
                                    listaPaketa.add(paket.getIdP());
                                }
                            }
                        }
                    } else {
                        if (paket.getAdresaPolaska().getIdG().getIdG() == cityId) {
                            listaPaketa.add(paket.getIdP());
                        }
                    }
                }
            });
            return listaPaketa;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean deletePackage(int packageId) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return false;
            }
            if (paket.getStatus() == 0 || paket.getStatus() == 4) {
                return false;
            }
            JPAClass.em.getTransaction().begin();
            JPAClass.em.remove(paket);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean changeWeight(int packageId, BigDecimal newWeight) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return false;
            }
            if (paket.getStatus() != 0) {
                return false;
            }
            paket.setTezina(newWeight);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(paket);
            JPAClass.em.getTransaction().commit();
            JPAClass.em.refresh(paket);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean changeType(int packageId, int newType) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return false;
            }
            if (paket.getStatus() != 0) {
                return false;
            }
            paket.setTipPaketa(newType);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(paket);
            JPAClass.em.getTransaction().commit();
            JPAClass.em.refresh(paket);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int getDeliveryStatus(int packageId) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return -1;
            }
            return paket.getStatus();
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public BigDecimal getPriceOfDelivery(int packageId) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return null;
            }
            return paket.getCena();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public int getCurrentLocationOfPackage(int packageId) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return -2;
            }
            if (paket.getIdV() != null) {
                return -1;
            }
            if (paket.getStatus() == 3) {
                return paket.getAdresaDolaska().getIdG().getIdG();
            }
            if (paket.getTrenutnaLokacija() != null) {
                return paket.getTrenutnaLokacija().getIdG().getIdG();
            }
            return paket.getAdresaPolaska().getIdG().getIdG();
        } catch (Exception e) {
            return -2;
        }
    }

    @Override
    public Date getAcceptanceTime(int packageId) {
        try {
            Paket paket = JPAClass.em.find(Paket.class, packageId);
            if (paket == null) {
                return null;
            }
            return new Date(paket.getVremePrihvatanja().getTime());
        } catch (Exception e) {
            return null;
        }
    }

}
