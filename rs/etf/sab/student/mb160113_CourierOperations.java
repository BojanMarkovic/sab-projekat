package rs.etf.sab.student;

import rs.etf.sab.operations.CourierOperations;
import rs.etf.sab.student.entiteti.Korisnik;
import rs.etf.sab.student.entiteti.Kurir;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class mb160113_CourierOperations implements CourierOperations {

    @Override
    public boolean insertCourier(String courierUserName, String driverLicenceNumber) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUserName).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Kurir kurir = new Kurir();
            kurir.setKorisnik(korisnik);
            kurir.setIdK(korisnik.getIdK());
            kurir.setBrojVozackeDozvole(driverLicenceNumber);
            kurir.setBrojIsporucenihPaketa(0);
            kurir.setProfit(new BigDecimal(BigInteger.ZERO));
            kurir.setStatus(0);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(kurir);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean deleteCourier(String courierUserName) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUserName).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Kurir kurir = JPAClass.em.createNamedQuery("Kurir.findByIdK", Kurir.class).setParameter("idK", korisnik.getIdK()).getSingleResult();
            if (kurir == null) {
                return false;
            }
            JPAClass.em.getTransaction().begin();
            JPAClass.em.remove(kurir);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<String> getCouriersWithStatus(int statusOfCourier) {
        try {
            List<Kurir> kuriri = JPAClass.em.createNamedQuery("Kurir.findByStatus", Kurir.class).setParameter("status", statusOfCourier).getResultList();
            if (kuriri == null) {
                return null;
            }
            List<String> imenaKurira = new ArrayList<>();
            kuriri.forEach(kurir -> imenaKurira.add(kurir.getKorisnik().getKorisnickoIme()));
            return imenaKurira;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<String> getAllCouriers() {
        try {
            List<Kurir> kuriri = JPAClass.em.createNamedQuery("Kurir.findAll", Kurir.class).getResultList();
            if (kuriri == null) {
                return null;
            }
            List<String> imenaKurira = new ArrayList<>();
            kuriri.sort((k1, k2) -> k2.getProfit().compareTo(k1.getProfit()));
            kuriri.forEach(kurir -> imenaKurira.add(kurir.getKorisnik().getKorisnickoIme()));
            return imenaKurira;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public BigDecimal getAverageCourierProfit(int numberOfDeliveries) {
        try {
            BigDecimal sum = new BigDecimal(BigInteger.ZERO);
            List<Kurir> kuriri;
            if (numberOfDeliveries == -1) {
                kuriri = JPAClass.em.createNamedQuery("Kurir.findAll", Kurir.class).getResultList();
            } else {
                kuriri = JPAClass.em.createNamedQuery("Kurir.findByBrojIsporucenihPaketa", Kurir.class).setParameter("brojIsporucenihPaketa", numberOfDeliveries).getResultList();
            }
            if (kuriri == null || kuriri.isEmpty()) {
                return null;
            }
            for (Kurir kurir : kuriri) {
                sum = sum.add(kurir.getProfit());
            }
            return sum.divide(BigDecimal.valueOf(kuriri.size()));
        } catch (Exception e) {
            return null;
        }
    }

}
