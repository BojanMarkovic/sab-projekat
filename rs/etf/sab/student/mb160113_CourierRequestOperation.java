package rs.etf.sab.student;

import rs.etf.sab.operations.CourierRequestOperation;
import rs.etf.sab.student.entiteti.Korisnik;
import rs.etf.sab.student.entiteti.Kurir;
import rs.etf.sab.student.entiteti.Zahtevi;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class mb160113_CourierRequestOperation implements CourierRequestOperation {

    @Override
    public boolean insertCourierRequest(String userName, String licencePlateNumber) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Kurir kurir = JPAClass.em.find(Kurir.class, korisnik.getIdK());
            if (kurir != null) {
                return false;
            }
            List<Kurir> kuriri = JPAClass.em.createNamedQuery("Kurir.findByBrojVozackeDozvole", Kurir.class).setParameter("brojVozackeDozvole", licencePlateNumber).getResultList();
            if (kuriri == null || kuriri.isEmpty()) {
                Zahtevi zahtev = new Zahtevi();
                zahtev.setBrojVozackeDozvole(licencePlateNumber);
                zahtev.setKorisnik(korisnik);
                zahtev.setIdK(korisnik.getIdK());
                JPAClass.em.getTransaction().begin();
                JPAClass.em.persist(zahtev);
                JPAClass.em.getTransaction().commit();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean deleteCourierRequest(String userName) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Zahtevi zahtev = JPAClass.em.find(Zahtevi.class, korisnik.getIdK());
            if (zahtev == null) {
                return false;
            }
            JPAClass.em.getTransaction().begin();
            JPAClass.em.remove(zahtev);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public List<String> getAllCourierRequests() {
        try {
            List<Zahtevi> zahtevi = JPAClass.em.createNamedQuery("Zahtevi.findAll", Zahtevi.class).getResultList();
            if (zahtevi == null) {
                return null;
            }
            List<String> imena = new ArrayList<>();
            zahtevi.forEach(zahtev -> imena.add(zahtev.getKorisnik().getKorisnickoIme()));
            return imena;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean grantRequest(String userName) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", userName).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Zahtevi zahtev = JPAClass.em.find(Zahtevi.class, korisnik.getIdK());
            if (zahtev == null) {
                return false;
            }
            Kurir kurir = new Kurir();
            kurir.setIdK(korisnik.getIdK());
            kurir.setKorisnik(korisnik);
            kurir.setBrojVozackeDozvole(zahtev.getBrojVozackeDozvole());
            kurir.setBrojIsporucenihPaketa(0);
            kurir.setProfit(new BigDecimal(BigInteger.ZERO));
            kurir.setStatus(0);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.remove(zahtev);
            JPAClass.em.persist(kurir);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean changeDriverLicenceNumberInCourierRequest(String courierUserName, String driverLicenceNumber) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUserName).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Zahtevi zahtev = JPAClass.em.find(Zahtevi.class, korisnik.getIdK());
            if (zahtev == null) {
                return false;
            }
            zahtev.setBrojVozackeDozvole(driverLicenceNumber);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(zahtev);
            JPAClass.em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
