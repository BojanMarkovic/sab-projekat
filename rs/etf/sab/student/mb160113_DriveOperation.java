package rs.etf.sab.student;

import rs.etf.sab.operations.DriveOperation;
import rs.etf.sab.student.entiteti.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class mb160113_DriveOperation implements DriveOperation {

    private BigDecimal distance(double xkoordinata1, double xkoordinata2, double ykoordinata1, double ykoordinata2) {
        double deltaX = xkoordinata1 - xkoordinata2;
        double deltaY = ykoordinata1 - ykoordinata2;
        double result = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        return BigDecimal.valueOf(result);
    }

    private BigDecimal racunajKolikoCeMeKostati(Kurir kurir, Adresa lokacija1, Adresa lokacija2) {
        int gorivo = 0;
        switch (kurir.getIdV().getTipGoriva()) {
            case 0:
                gorivo = 15;
                break;
            case 1:
                gorivo = 32;
                break;
            case 2:
                gorivo = 36;
                break;
        }
        double xKoor1 = lokacija1.getXkoordinata();
        double xKoor2 = lokacija2.getXkoordinata();
        double yKoor1 = lokacija1.getYkoordinata();
        double yKoor2 = lokacija2.getYkoordinata();
        return distance(xKoor1, xKoor2, yKoor1, yKoor2).multiply(BigDecimal.valueOf(gorivo)).multiply(kurir.getIdV().getPotrosnja());
    }

    private BigDecimal odradiDrugeGradove(Kurir kurir, BigDecimal tempTezina, Grad grad) {
        List<LokacijaMagacina> temp = JPAClass.em.createQuery("SELECT l FROM LokacijaMagacina l WHERE l.idA.idG.idG = :grad", LokacijaMagacina.class)
                .setParameter("grad", grad.getIdG()).getResultList();
        Adresa magacinAdresa2 = null;
        List<Paket> paketi;
        if (temp == null || temp.isEmpty()) {
            paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE p.status = :status AND p.adresaPolaska.idG.idG = :grad", Paket.class)
                    .setParameter("status", 1).setParameter("grad", grad.getIdG()).getResultList();
        } else {
            magacinAdresa2 = temp.get(0).getIdA();
            paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE (p.status = :status OR p.status = :status2) AND (p.adresaPolaska.idG.idG = :grad OR p.trenutnaLokacija.idA = :idA)AND p.potrebaZaDostavom=NULL", Paket.class)
                    .setParameter("status", 1).setParameter("status2", 2).setParameter("grad", grad.getIdG()).setParameter("idA", magacinAdresa2.getIdA()).getResultList();
        }
        for (Paket paket2 : paketi) {
            if (paket2.getTrenutnaLokacija() != null) {
                continue;
            }
            if (paket2.getAdresaPolaska() != magacinAdresa2) {
                if (tempTezina.add(paket2.getTezina()).compareTo(kurir.getIdV().getNosivost()) < 0) {
                    tempTezina = tempTezina.add(paket2.getTezina());
                    Ruta ruta = new Ruta();
                    paket2.setPotrebaZaDostavom(-1);
                    ruta.setIdA(paket2.getAdresaPolaska());
                    ruta.setIdK(kurir);
                    JPAClass.em.getTransaction().begin();
                    JPAClass.em.persist(paket2);
                    JPAClass.em.persist(ruta);
                    JPAClass.em.getTransaction().commit();
                }
            }
        }
        if (magacinAdresa2 != null) {
            for (Paket paket2 : paketi) {
                if ((paket2.getAdresaPolaska() == magacinAdresa2) || (paket2.getTrenutnaLokacija() == magacinAdresa2)) {
                    if (tempTezina.add(paket2.getTezina()).compareTo(kurir.getIdV().getNosivost()) < 0) {
                        tempTezina = tempTezina.add(paket2.getTezina());
                        Ruta ruta = new Ruta();
                        paket2.setPotrebaZaDostavom(-1);
                        if (paket2.getTrenutnaLokacija() == magacinAdresa2) {
                            ruta.setIdA(paket2.getTrenutnaLokacija());
                        } else {
                            ruta.setIdA(paket2.getAdresaPolaska());
                        }
                        ruta.setIdK(kurir);
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.persist(paket2);
                        JPAClass.em.persist(ruta);
                        JPAClass.em.getTransaction().commit();
                    }
                }
            }
        }
        return tempTezina;
    }

    private void ocistiDuplikateRuta() {
        List<Ruta> rute = JPAClass.em.createNamedQuery("Ruta.findAll", Ruta.class).getResultList();
        for (int i = 0; i < rute.size() - 1; i++) {
            for (int j = i + 1; j < rute.size(); j++) {
                if ((rute.get(i).getIdA() == rute.get(j).getIdA()) && (rute.get(i).getIdK() == rute.get(j).getIdK())) {
                    JPAClass.em.getTransaction().begin();
                    JPAClass.em.remove(rute.get(j));
                    JPAClass.em.getTransaction().commit();
                }
            }
        }
    }

    @Override
    public boolean planingDrive(String courierUsername) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUsername).getSingleResult();
            if (korisnik == null) {
                return false;
            }
            Kurir kurir = JPAClass.em.find(Kurir.class, korisnik.getIdK());
            if (kurir == null) {
                return false;
            }
            if (kurir.getIdV() == null) {
                List<Vozilo> vozila = JPAClass.em.createQuery("SELECT v FROM Vozilo v WHERE v.idM.idA.idG.idG = :grad", Vozilo.class).setParameter("grad", korisnik.getAdresa().getIdG().getIdG()).getResultList();
                for (Vozilo vozilo : vozila) {
                    List<Kurir> kuriri = JPAClass.em.createQuery("SELECT k FROM Kurir k WHERE k.idV.idV = :vozilo", Kurir.class).setParameter("vozilo", vozilo.getIdV()).getResultList();
                    if (kuriri != null) {
                        if (kuriri.isEmpty()) {
                            kurir.setIdV(vozilo);
                            break;
                        }
                    } else {
                        kurir.setIdV(vozilo);
                        break;
                    }
                }
            }
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(kurir);
            JPAClass.em.getTransaction().commit();
            if (kurir.getIdV() == null) {
                return false;
            }
            kurir.setTempProfit(BigDecimal.ZERO);
            kurir.getIdV().setZauzet(BigDecimal.ZERO);
            kurir.getIdV().setIdM(null);
            Adresa magacinAdresa = JPAClass.em.createQuery("SELECT l FROM LokacijaMagacina l WHERE l.idA.idG.idG = :grad", LokacijaMagacina.class)
                    .setParameter("grad", korisnik.getAdresa().getIdG().getIdG()).getSingleResult().getIdA();

            List<Paket> paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE (p.status = :status OR p.status = :status2) AND (p.adresaPolaska.idG.idG = :grad OR p.trenutnaLokacija.idA = :idA) AND p.potrebaZaDostavom=NULL", Paket.class)
                    .setParameter("status", 1).setParameter("status2", 2).setParameter("grad", korisnik.getAdresa().getIdG().getIdG()).setParameter("idA", magacinAdresa.getIdA()).getResultList();
            if (paketi == null || paketi.isEmpty()) {
                kurir.setIdV(null);
                return false;
            }

            BigDecimal tempTezina = BigDecimal.ZERO;

            List<Paket> paketiMoji = new ArrayList<>();
            Ruta lastRuta = null;
            for (Paket paket : paketi) {
                if (paket.getTrenutnaLokacija() != null) {
                    continue;
                }
                if (paket.getAdresaPolaska() != magacinAdresa) {
                    if (tempTezina.add(paket.getTezina()).compareTo(kurir.getIdV().getNosivost()) < 0) {
                        paketiMoji.add(paket);
                        Ruta ruta = new Ruta();
                        ruta.setIdA(paket.getAdresaPolaska());
                        ruta.setIdK(kurir);
                        paket.setPotrebaZaDostavom(1);
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.persist(paket);
                        JPAClass.em.persist(ruta);
                        JPAClass.em.persist(kurir);
                        JPAClass.em.getTransaction().commit();
                        lastRuta = ruta;
                    }
                }
            }
            for (Paket paket : paketi) {
                if ((paket.getAdresaPolaska() == magacinAdresa) || (paket.getTrenutnaLokacija() == magacinAdresa)) {
                    if (tempTezina.add(paket.getTezina()).compareTo(kurir.getIdV().getNosivost()) < 0) {
                        paketiMoji.add(paket);
                        Ruta ruta = new Ruta();
                        if (paket.getTrenutnaLokacija() == magacinAdresa) {
                            ruta.setIdA(paket.getTrenutnaLokacija());
                        } else {
                            ruta.setIdA(paket.getAdresaPolaska());
                        }
                        ruta.setIdK(kurir);
                        paket.setPotrebaZaDostavom(1);
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.persist(paket);
                        JPAClass.em.persist(ruta);
                        JPAClass.em.persist(kurir);
                        JPAClass.em.getTransaction().commit();
                        lastRuta = ruta;
                    }
                }
            }

            kurir.setTempProfit(BigDecimal.ZERO.subtract(racunajKolikoCeMeKostati(kurir, korisnik.getAdresa(), paketiMoji.get(0).getAdresaPolaska())));
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(kurir);
            JPAClass.em.getTransaction().commit();

            Adresa korisnikAdresa = null;
            if (lastRuta != null) {
                korisnikAdresa = lastRuta.getIdA();
            } else {
                korisnikAdresa = korisnik.getAdresa();
            }
            Grad grad = null;
            boolean flag = true;
            while (!paketiMoji.isEmpty()) {
                Paket paket = paketiMoji.get(0);
                BigDecimal min = distance(korisnikAdresa.getXkoordinata(), paket.getAdresaDolaska().getXkoordinata(),
                        korisnikAdresa.getYkoordinata(), paket.getAdresaDolaska().getYkoordinata());
                Paket index = paket;
                for (Paket paket3 : paketiMoji) {
                    if (paket3 != paket) {
                        BigDecimal temp = distance(korisnikAdresa.getXkoordinata(), paket3.getAdresaDolaska().getXkoordinata(),
                                korisnikAdresa.getYkoordinata(), paket3.getAdresaDolaska().getYkoordinata());
                        if (min.compareTo(temp) > 0) {
                            min = temp;
                            index = paket3;
                        }
                    }
                }
                if (flag) {
                    grad = index.getAdresaDolaska().getIdG();
                    flag = false;
                }
                if (grad != index.getAdresaDolaska().getIdG()) {
                    tempTezina = odradiDrugeGradove(kurir, tempTezina, grad);
                    grad = index.getAdresaDolaska().getIdG();
                }

                Ruta ruta = new Ruta();
                ruta.setIdA(index.getAdresaDolaska());
                ruta.setIdK(kurir);
                JPAClass.em.getTransaction().begin();
                JPAClass.em.persist(ruta);
                JPAClass.em.getTransaction().commit();
                tempTezina = tempTezina.subtract(index.getTezina());
                korisnikAdresa = index.getAdresaDolaska();
                paketiMoji.remove(index);
            }
            if (grad != null) {
                odradiDrugeGradove(kurir, tempTezina, grad);
            }
            ocistiDuplikateRuta();

            Ruta ruta = new Ruta();
            ruta.setIdA(magacinAdresa);
            ruta.setIdK(kurir);
            kurir.setStatus(1);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(ruta);
            JPAClass.em.persist(kurir);
            JPAClass.em.getTransaction().commit();

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public int nextStop(String courierUsername) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUsername).getSingleResult();
            if (korisnik == null) {
                return -3;
            }
            Kurir kurir = JPAClass.em.find(Kurir.class, korisnik.getIdK());
            if (kurir == null) {
                return -3;
            }
            List<Ruta> rute = JPAClass.em.createQuery("SELECT r FROM Ruta r WHERE r.idK.idK = :idK", Ruta.class).setParameter("idK", korisnik.getIdK()).getResultList();
            if (rute == null || rute.isEmpty()) {
                return -3;
            }

            List<Paket> paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE p.idV.idV = :idV", Paket.class).setParameter("idV", kurir.getIdV().getIdV()).getResultList();
            int idPaketa = -2;
            int brojPaketa = 0;
            if (!paketi.isEmpty()) {
                Adresa adresaTrenutna = rute.get(0).getIdA();
                for (Paket paket : paketi) {
                    paket.setTrenutnaLokacija(adresaTrenutna);
                    if (paket.getPotrebaZaDostavom() == -1) {
                        continue;
                    }
                    if (paket.getAdresaDolaska().equals(adresaTrenutna)) {
                        brojPaketa++;

                        idPaketa = paket.getIdP();
                        paket.setStatus(3);
                        paket.setIdV(null);
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.persist(paket);
                        JPAClass.em.getTransaction().commit();
                        kurir.getIdV().setZauzet(kurir.getIdV().getZauzet().subtract(paket.getTezina()));
                        kurir.setTempProfit(kurir.getTempProfit().add(paket.getCena()));
                    }
                }
            }

            List<Paket> paketiUzmi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE ((p.status = :status AND p.adresaPolaska.idA = :idA) OR (p.status = :status2 AND p.trenutnaLokacija.idA = :idA2)) AND p.potrebaZaDostavom IS NOT NULL", Paket.class)
                    .setParameter("status", 1).setParameter("idA", rute.get(0).getIdA().getIdA()).setParameter("status2", 2).setParameter("idA2", rute.get(0).getIdA().getIdA()).getResultList();
            for (Paket paket : paketiUzmi) {
                if (kurir.getIdV().getNosivost().subtract(kurir.getIdV().getZauzet()).compareTo(paket.getTezina()) > 0) {
                    paket.setStatus(2);
                    if (paket.getPotrebaZaDostavom() == null) {
                        paket.setPotrebaZaDostavom(-1);
                    }
                    kurir.getIdV().setZauzet(kurir.getIdV().getZauzet().subtract(paket.getTezina()));
                    paket.setIdV(kurir.getIdV());
                    paket.setTrenutnaLokacija(paket.getAdresaPolaska());
                    JPAClass.em.getTransaction().begin();
                    JPAClass.em.persist(paket);
                    JPAClass.em.getTransaction().commit();
                }
            }

            if (rute.size() == 1) {
                paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE p.idV.idV = :idV", Paket.class).setParameter("idV", kurir.getIdV().getIdV()).getResultList();
                if (!paketi.isEmpty()) {
                    for (Paket paket : paketi) {
                        paket.setIdV(null);
                        paket.setPotrebaZaDostavom(null);
                        JPAClass.em.getTransaction().begin();
                        JPAClass.em.persist(paket);
                        JPAClass.em.getTransaction().commit();
                    }
                    kurir.getIdV().setZauzet(BigDecimal.ZERO);
                }
                LokacijaMagacina magacin = JPAClass.em.createQuery("SELECT l FROM LokacijaMagacina l WHERE l.idA.idA = :idA", LokacijaMagacina.class).setParameter("idA", rute.get(0).getIdA().getIdA()).getSingleResult();
                kurir.getIdV().setIdM(magacin);
                kurir.setIdV(null);
                idPaketa = -1;
                kurir.setStatus(0);
                kurir.setProfit(kurir.getTempProfit());
                kurir.setTempProfit(BigDecimal.ZERO);
            } else {
                kurir.setTempProfit(kurir.getTempProfit().subtract(racunajKolikoCeMeKostati(kurir, rute.get(0).getIdA(), rute.get(1).getIdA())));
            }
            kurir.setBrojIsporucenihPaketa(kurir.getBrojIsporucenihPaketa() + brojPaketa);
            JPAClass.em.getTransaction().begin();
            JPAClass.em.persist(kurir);
            JPAClass.em.remove(rute.get(0));
            JPAClass.em.getTransaction().commit();
            return idPaketa;
        } catch (Exception e) {
            return -3;
        }
    }

    @Override
    public List<Integer> getPackagesInVehicle(String courierUsername) {
        try {
            Korisnik korisnik = JPAClass.em.createNamedQuery("Korisnik.findByKorisnickoIme", Korisnik.class).setParameter("korisnickoIme", courierUsername).getSingleResult();
            if (korisnik == null) {
                return null;
            }
            Kurir kurir = JPAClass.em.find(Kurir.class, korisnik.getIdK());
            if (kurir == null) {
                return null;
            }
            List<Paket> paketi = JPAClass.em.createQuery("SELECT p FROM Paket p WHERE p.idV.idV = :idV", Paket.class).setParameter("idV", kurir.getIdV().getIdV()).getResultList();
            if (paketi == null) {
                return null;
            }
            List<Integer> paketiKljucevi = new ArrayList<>();
            paketi.forEach(paket -> paketiKljucevi.add(paket.getIdP()));
            return paketiKljucevi;
        } catch (Exception e) {
            return null;
        }
    }

}
