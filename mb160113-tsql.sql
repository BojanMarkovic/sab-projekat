CREATE TRIGGER VoziloJedinstvenoKurir
   ON  kurir
   AFTER UPDATE
AS 
BEGIN
	DECLARE @kursor CURSOR
	DECLARE @id INT

	SET @kursor = CURSOR FORWARD_ONLY FOR
	SELECT IdV
	FROM INSERTED

	OPEN @kursor

	FETCH NEXT FROM @kursor INTO @id

	WHILE @@fetch_status =0
	BEGIN
		IF (SELECT COUNT(*) FROM dbo.kurir WHERE ISNULL(IdV,-1) = @id)>1 
		BEGIN
			ROLLBACK
			BREAK
		END

		FETCH NEXT FROM @kursor INTO @id
	end

	CLOSE @kursor
	DEALLOCATE @kursor

END
GO

CREATE TRIGGER VozioTabela
   ON  kurir
   AFTER UPDATE
AS 
BEGIN
	DECLARE @kursor CURSOR
	DECLARE @id INT
	DECLARE @idK INT

	SET @kursor = CURSOR FORWARD_ONLY FOR
	SELECT IdV, IdK
	FROM DELETED

	OPEN @kursor

	FETCH NEXT FROM @kursor INTO @id, @IdK

	WHILE @@fetch_status =0
	BEGIN
		IF @id IS NOT NULL 
		BEGIN
			IF NOT EXISTS (SELECT * FROM vozio WHERE IdK=@idK AND IdV=@id)
			BEGIN
				INSERT INTO vozio (IdK, IdV) VALUES (@idK, @id)
			END
		END

		FETCH NEXT FROM @kursor INTO @id, @IdK
	end

	CLOSE @kursor
	DEALLOCATE @kursor

END
GO


CREATE TRIGGER TR_TransportOffer
   ON  Paket
   AFTER INSERT, UPDATE
AS 
BEGIN
	DECLARE @kursor CURSOR
	DECLARE @idP INT
	DECLARE @tip INT
	DECLARE @idPolaska INT
	DECLARE @idDolaska INT
	DECLARE @tezina DECIMAL(10,3)
	DECLARE @xPolaska INT
	DECLARE @yPolaska INT
	DECLARE @xDolaska INT
	DECLARE @yDolaska INT

	SET @kursor = CURSOR FOR
	SELECT IdP, adresa_dolaska, adresa_polaska, tip_paketa, tezina
	FROM inserted

	OPEN @kursor

	FETCH NEXT FROM @kursor INTO @idP, @idDolaska, @idPolaska, @tip, @tezina

	WHILE @@FETCH_STATUS=0
	begin
		SELECT @xDolaska=Xkoordinata, @yDolaska=Ykoordinata
		FROM Adresa
		WHERE IdA=@idDolaska

		SELECT @xPolaska=Xkoordinata, @yPolaska=Ykoordinata
		FROM Adresa
		WHERE IdA=@idPolaska

		UPDATE Paket 
		SET cena=cast(sqrt((power(@xPolaska-@xDolaska,2))+(power(@yPolaska-@yDolaska,2))) as decimal(10,3)) * (
		CASE WHEN @tip = 0 THEN 115
		WHEN @tip = 1 THEN (175+@tezina*100)
		WHEN @tip = 2 THEN (250+@tezina*100)
		WHEN @tip = 3 THEN (350+@tezina*500)
		END)
		WHERE IdP=@idP

		FETCH NEXT FROM @kursor INTO @idP, @idDolaska, @idPolaska, @tip, @tezina
	end


	CLOSE @kursor
	DEALLOCATE @kursor

END
GO
