USE [kurirskaSluzba]
GO

EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'
EXEC sp_MSForEachTable 'DELETE FROM ?'
EXEC sp_MSForEachTable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL'
DBCC CHECKIDENT ('Grad', RESEED, 0);
DBCC CHECKIDENT ('Adresa', RESEED, 0);
DBCC CHECKIDENT ('Korisnik', RESEED, 0);
DBCC CHECKIDENT ('LokacijaMagacina', RESEED, 0);
DBCC CHECKIDENT ('Paket', RESEED, 0);
DBCC CHECKIDENT ('Vozilo', RESEED, 0);

INSERT INTO [dbo].[Grad]
           ([Naziv],[Postanski_broj])
	VALUES
           ('Grad1',11000),
           ('Grad2',21000),
           ('Grad3',31000),
           ('Grad4',41000),
           ('Grad5',51000),
           ('Grad6',61000)
GO


INSERT INTO [dbo].[Adresa]
           ([Ulica],[Broj],[Xkoordinata],[Ykoordinata],[IdG])
     VALUES
            ('Ulica1',1,5.3,5.3,1),
			('Ulica2',2,4.3,2.3,1),
			('Ulica3',3,3.3,5.0,1),
			('Ulica4',4,9.3,5.9,1),
			('Ulica5',5,15.3,7.5,1),
			('Ulica6',6,10.3,8,1),
			('Ulica7',7,20.3,-10.0,2),
			('Ulica8',8,-5.3,2.2,1),
			('Ulica9',9,0.3,2.9,1),
			('Ulica10',10,-1.3,4.8,1),
			('Ulica11',11,55.3,25.3,3),
			('Ulica12',12,65.3,28.3,3),
			('Ulica13',13,105.3,79.3,4),
			('Ulica14',14,109.3,78.3,4),
			('Ulica15',15,75.3,24.3,3),
			('Ulica16',16,-55.3,-55.3,5),
			('Ulica17',17,-105.3,-68.3,6),
			('Ulica18',18,-102.3,-70.3,6)
GO

INSERT INTO [dbo].[LokacijaMagacina]
           ([IdA])
     VALUES(1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),
		(16),(18),(15),(17)
GO

INSERT INTO [dbo].[Korisnik]
           ([ime],[prezime],[korisnicko_ime],[sifra],[adresa])
     VALUES
           ('Ime1','prezime1','Kor1','test',1),
           ('Ime2','prezime2','Kor2','test',2),
           ('Ime3','prezime3','Kor3','test',3),
           ('Ime4','prezime4','Kor4','test',8),
           ('Ime5','prezime5','Kor5','test',9),
           ('Ime6','prezime6','Kor6','test',10),
           ('Ime7','prezime7','Kor7','test',11),
           ('Ime8','prezime8','Kor8','test',15),
           ('Ime9','prezime9','Kor9','test',17),
           ('Ime10','prezime10','Kor10','test',5),
           ('Ime11','prezime11','Kor11','test',1),
           ('Ime12','prezime12','Kor12','test',2),
           ('Ime13','prezime13','Kor13','test',10),
           ('Ime14','prezime14','Kor14','test',7),
           ('Ime15','prezime15','Kor15','test',12),
           ('Ime16','prezime16','Kor16','test',12),
           ('Ime17','prezime17','Kor17','test',2),
           ('Ime18','prezime18','Kor18','test',1),
           ('Ime19','prezime19','Kor19','test',8),
           ('Ime20','prezime20','Kor20','test',6)

GO

INSERT INTO [dbo].[kurir]
           ([IdK],[broj_isporucenih_paketa],[profit],[status],[IdV],[broj_vozacke_dozvole])
     VALUES
           (10,5,10.456,0,NULL,'1'),
           (11,0,0.456,0,NULL,'2'),
		   (12,1,1.456,0,NULL,'3'),
		   (13,6,60.456,0,NULL,'4'),
		   (14,20,110.456,0,NULL,'5'),
		   (15,51,19.456,0,NULL,'6'),
           (16,13,50.456,0,NULL,'7'),
		   (17,9,113.456,0,NULL,'8'),
		   (18,7,1.96,0,NULL,'9')
		   
GO

INSERT INTO [dbo].[Vozilo]
           ([registracioni_broj],[tip_goriva],[potrosnja],[nosivost],[IdM])
     VALUES
           ('456',0,1.2,50.3,1),
		   ('542',1,13.2,10.3,2),
		   ('345',2,5.2,2.3,3),
		   ('44535',0,0.2,5.3,4),
		   ('54345',1,9.2,500.3,5),
		   ('44556',2,1.5,0.3,6),
		   ('4126',0,1.6,18.3,7),
		   ('45879',1,1.8,55.3,8),
		   ('47356',2,4,50.9,9),
		   ('41535',0,3.2,59.3,10),
		   ('54245',1,7.2,45.3,11),
		   ('21456',2,9.2,21.3,12),
		   ('24456',0,3.5,35.3,13),
		   ('4156',1,3.8,12.58,14),
		   ('4506',2,5.1,38.9,15),
		   ('4956',0,0.88,453.78,16),
		   ('4056',1,7.65,1000,17),
		   ('40786',2,4.23,5,18)
GO

INSERT INTO [dbo].[Paket]
           ([tip_paketa],[tezina],[cena],[vreme_kreiranja],[vreme_prihvatanja],
		   [adresa_polaska],[adresa_dolaska],[trenutna_lokacija],[status],[IdV],[posiljalac])
     VALUES
           (0,0.2,5,'20200521 10:34:09 AM',NULL,1,16,NULL,0,NULL,1),
		   (1,50.3,50,'20200522 11:34:19 AM',NULL,2,18,NULL,0,NULL,2),
		   (2,100.22,15,'20200523 12:34:29 AM',NULL,3,18,NULL,0,NULL,1),
		   (3,6.25,25,'20200524 01:34:39 AM',NULL,4,17,NULL,0,NULL,3),
		   (0,2.27,35,'20200525 02:34:49 AM',NULL,5,16,NULL,0,NULL,2),
		   (1,5.35,5.45,'20200526 03:34:59 AM',NULL,6,15,NULL,0,NULL,3),
		   (2,9.21,5.2,'20200527 04:34:12 AM',NULL,7,14,NULL,0,NULL,4),
		   (3,12.96,9,'20200528 05:34:13 AM',NULL,8,13,NULL,0,NULL,5),
		   (0,13.36,13,'20200529 06:34:21 AM',NULL,9,12,NULL,0,NULL,4),
		   (1,20.12,12,'20200121 07:34:10 AM',NULL,10,11,NULL,0,NULL,5),
		   (2,36.87,11,'20200221 08:34:35 AM',NULL,11,10,NULL,0,NULL,1),
		   (3,48.36,87,'20200321 09:34:45 AM',NULL,12,9,NULL,0,NULL,2),
		   (0,9.21,0.5,'20200421 10:24:38 PM',NULL,13,8,NULL,0,NULL,3),
		   (1,7.69,29,'20200621 10:14:02 PM',NULL,14,7,NULL,0,NULL,4),
		   (2,6.15,26,'20200511 10:44:01 PM',NULL,15,6,NULL,0,NULL,5),
		   (3,19.78,23,'20200512 10:54:03 PM',NULL,16,5,NULL,0,NULL,6),
		   (0,18.63,12.85,'20200513 10:31:05 PM',NULL,17,4,NULL,0,NULL,6),
		   (1,6.36,12.5,'20200514 10:32:06 PM',NULL,18,3,NULL,0,NULL,1),
		   (2,2.96,4.98,'20200511 10:33:07 PM',NULL,5,2,NULL,0,NULL,2),
		   (3,1.15,20.47,'20200515 10:38:08 PM',NULL,14,1,NULL,0,NULL,3)
GO


